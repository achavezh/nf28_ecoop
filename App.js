import React from 'react';

import Footer from './Components/Menu/Footer'
import { AuthProvider } from './Context/AuthContext';
import Login from './Pages/Home/Login'

import { LogBox } from 'react-native';
LogBox.ignoreLogs(['Warning: ...']); // Ignore log notification by message
LogBox.ignoreAllLogs();//Ignore all log notifications

const App = () => {
  return (
    <AuthProvider>
        <Footer />
    </AuthProvider>
  );
};

export default App;
