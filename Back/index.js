// server/index.js

const express = require("express");

const PORT = process.env.PORT || 3001;

const app = express();

var mysql = require('mysql');
var session = require('express-session');
var path = require('path');
var cors = require('cors')

app.use(express.json())
app.use(session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true,
    cookie: { secure: true , maxAge:900000}
}));
var con = mysql.createConnection({
    // Connexion MySQL sur VPS
    host:'localhost',
    port:'3306', 
    user:'nf28',
    password:'NF28nf28$',
    database: 'nf28_ecoop'

    /*
    host:'tuxa.sme.utc',
    port:'3306', //port de MySQL en XAMPP
    user:'sr03p058',
    password:'cqSQ6lZi', //'' pour Windows
    database: 'sr03p058'
    */
});

app.use(cors());
app.get('/nf28', function(req, res){
    con.query('select * from Conseil where categoryName="'+req.query.category+'"', function(error, rows, fields){
        if (error) console.log(error);

        else {
            //console.log(rows);
            res.json(rows)
        }

    });
});

app.post('/creerEvent', function(req, res){
    const participant = req.body.participant
    const type=req.body.type
    const adresse=req.body.adresse
    const ville=req.body.ville
    const description=req.body.description
    const code=req.body.codePost
    const mail=req.body.mail
    const date=req.body.date
    const date2=new Date(date)
    var datetime = date2.toISOString().replace(/T/, ' ').replace(/\..+/, '')
    if(participant && type && adresse && ville && description && code) {
        con.query('INSERT INTO Eventa(nbParticipants, beginsOn, descirption, likes, organizer, type, street, zipCODE, town) VALUES (?,?,?,?,?,?,?,?,?)',
            [participant, datetime, description, 0, mail, type, adresse, code, ville], function (error, rows, fields) {
                if (error) {
                    console.log(error);
                    res.json({ajoute:false})

                }
                else {
                    console.log(rows);
                    res.json({ajoute:true, id:rows.insertId})
                 }
            });
    }
    else {
        res.json({ajoute:false})
        res.end();
    }
});
app.post('/showEvent', function(request, response) {
    const id = request.body.id;
    const mail=request.body.mail
    console.log(id)
    if (id) {
        con.query('SELECT *,COUNT(Participate.participant) as part FROM Eventa LEFT JOIN Participate ON Eventa.idEvent=Participate.Event WHERE Eventa.idEvent = ? GROUP BY Participate.participant ', [id], function(error, results, fields) {
            if(error){
                console.log(error)
            }
            if (results.length > 0) {
                con.query('SELECT participant FROM Eventa LEFT JOIN Participate ON Eventa.idEvent=Participate.Event WHERE Eventa.idEvent = ? AND Participate.participant = ? ', [id,mail], function(error2, results2, fields) {
                    if(error2){
                        console.log(error2)
                    }
                    if (results2.length > 0) {
                        console.log(results)
                        results[0].partici=results2[0].participant
                        console.log(results)
                        response.json(results)
                    }
                    else{
                        console.log(results)
                        results[0].partici=false
                        console.log(results)
                        response.json(results)
                    }

                });

            } else {
                response.json({trouve:false });
            }
        });
    } else {
        response.json({message: 'Please enter mail and Password!'});
        response.end();
    }
});



app.post('/showAllEvent', function(request, response) {
    const mail=request.body.mail
    if(mail) {
        con.query('SELECT * FROM Eventa LEFT JOIN Participate ON Eventa.idEvent=Participate.Event LEFT JOIN Likes ON Eventa.idEvent=Likes.Event  WHERE (Participate.participant = ? OR Participate.participant is NULL) AND (Likes.userID = ? OR Likes.userID is NULL)', [ mail,mail], function (error, results, fields) {
            if(error){
                console.log(error)
            }
            if (results.length > 0) {
                response.json(results)
                console.log(results)
            }
             else {
                response.json({trouve:false });
            }
        });
    }

});


app.post('/showMyEvents', function(request, response) {
    const mail=request.body.mail
    if(mail) {
        con.query('SELECT * FROM Eventa WHERE organizer = ?',
            [mail], function (error, results, fields) {
            if(error){
                console.log(error)
            }
            if (results.length > 0) {
                response.json({success: true, res: results});

            }
             else {
                response.json({success:false });
            }
        });
    }

});
app.post('/showEventParti', function(request, response) {
    const mail=request.body.mail
    if(mail) {
        con.query('SELECT * FROM Eventa INNER JOIN Participate ON Eventa.idEvent=Participate.Event WHERE Participate.participant=?',
            [mail], function (error, results, fields) {
                if(error){
                    console.log(error)
                }
                if (results.length > 0) {
                    response.json({success: true, res: results});

                }
                else {
                    response.json({success:false });
                }
            });
    }

});


app.post('/inscrire', function(request, response) {
    const id = request.body.id;
    const mail=request.body.mail
    if(id && mail) {
        con.query('INSERT INTO Participate VALUES (?,?) ',[mail,id] ,function (error, results, fields) {
            if (error) {
                console.log(error)
            }
            console.log(results)
            if (results.affectedRows = 1) {

                response.json({fait:true})
            } else {
                response.json({fait:false})
            }
            response.end();
        });
    }

});

app.post('/desinscrire', function(request, response) {
    const id = request.body.id;
    const mail=request.body.mail
    if(id && mail) {
        con.query('DELETE  FROM Participate WHERE Event=? AND participant=? ',[id,mail] ,function (error, results, fields) {
            if (error) {
                console.log(error)
            }
            console.log(results)
            if (results.length > 0) {
                response.json({fait:true})
            } else {
                response.json({fait:false})
            }
            response.end();
        });
    }
});

app.post('/like', function(request, response) {
    const id = request.body.id;
    const mail=request.body.mail
    if(id && mail) {
        con.query('INSERT INTO Likes VALUES (?,?) ',[mail,id] ,function (error, results, fields) {
            if (error) {
                console.log(error)
            }
            console.log(results)
            if (results.affectedRows = 1) {

                response.json({fait:true})
            } else {
                response.json({fait:false})
            }
            response.end();
        });
    }

});

app.post('/unlike', function(request, response) {
    const id = request.body.id;
    const mail=request.body.mail
    if(id && mail) {
        con.query('DELETE  FROM Likes WHERE Event=? AND userID=? ',[id,mail] ,function (error, results, fields) {
            if (error) {
                console.log(error)
            }
            console.log(results)
            if (results.length > 0) {
                response.json({fait:true})
            } else {
                response.json({fait:false})
            }
            response.end();
        });
    }
});


app.post("/api", (req, res) => {
    res.send({ message: req.body.mail });
});
app.post('/auth2', (req, res, next) => {
    var mail = req.body.mail;
    var password = req.body.password;
    console.log(mail);
    console.log(password);

    con.query(
        'SELECT * FROM Duser WHERE mail = ? AND password = ?',
        [mail, password], function(error, results, fields) {
            if (error) {
                console.log(error);
                res.send( {'success': false, 'message': 'Could not connect to db'});
            }

            if (results.length > 0) {
                console.log("success");
                res.send( {'success': true, 'login': results[0].login, 'mail': results[0].mail });
            }
            else {
                console.log("not success");
                res.send( {'success': false, 'message': 'Invalid credentials'});
            }
        });
});

app.get('/home', function(request, response) {
    if (request.session.loggedin) {
        response.json({message: 'Welcome back, ' + request.session.mail + '!'});
    } else {
        response.json({message: 'Please login to view this page!'});
    }
    response.end();
});

app.post('/registerUser', function(request, response) {
    const username = request.body.username;
    const mail = request.body.mail;
    const password = request.body.password;
    const verifPassword = request.body.vpassword;

    if(password && verifPassword && mail && username){
        if (password == verifPassword) {
            if(password.length >= 8){
                con.query('SELECT * FROM Duser WHERE mail = ? AND login = ?', [mail, username], function(error, results, fields) {
                    console.log(results)
                    if (results.length == 0) {
                        con.query('INSERT INTO Duser(login, mail, password) VALUES (?,?,?)',
                        [username, mail, password], function (error, rows, fields) {
                            if (error) console.log(error);
                            else {
                                console.log(rows);
                                response.json({ajoute:true, status:200})
                            }
                        });
                    }else{
                        console.log("j suis là error unique name")
                        response.json({message: 'Le nom utilisateur et le mail existent déjà', ajoute:false, status:400});
                        response.end();
                    }
                });
            }else{
                response.json({message: 'Le mot de passe doit contenir au moins 8 caractères', ajoute: false, status:400});
                response.end();
            }

        } else {
            response.json({message: 'Le mot de passe et la vérification sont différentes', ajoute: false, status:400});
            response.end();
        }
    }else{
        response.json({message: 'Entrez tous les champs s\'il vous plaît', ajoute:false, status:400});
        response.end();
    }

});

app.post('/modifyUser', function(request, response) {
    const newusername = request.body.newusername;
    const newmail = request.body.newmail;
    const oldusername = request.body.oldusername;
    const oldmail = request.body.oldmail;

    if(newusername && newmail && oldmail && oldusername){
        con.query('SELECT * FROM Duser WHERE mail = ? AND login = ?', [newmail, newusername], function(error, results, fields) {
                if (results.length == 0) {
                    con.query('UPDATE Duser SET login = ?, mail = ? WHERE login = ? AND mail = ?',
                    [newusername, newmail, oldusername, oldmail,], function (error, rows, fields) {
                        if (error) console.log(error);
                        else {
                            console.log(rows);
                            response.json({ajoute:true, status:200, login: newusername, mail: newmail})
                        }
                    });
                }else{
                    response.json({message: 'Username and mail exist', ajoute:false, status:400});
                    console.log("Username and mail exist");
                    response.end();
                }
            });


    }else{
        response.json({message: 'Please enter all champs', ajoute:false, status:400});
        response.end();
    }

});

app.post('/changePassword', function(request, response) {
    const username = request.body.username;
    const mail = request.body.mail;
    const password = request.body.password;
    const verifPassword = request.body.vpassword;

    if(password && verifPassword && mail){
        if (password == verifPassword) {
            if(password.length >= 8){
                con.query('UPDATE Duser SET password = ? WHERE login = ? AND mail = ?',
                    [password, username, mail], function (error, rows, fields) {
                        if (error) console.log(error);
                        else {
                            console.log(rows);
                            response.json({ajoute:true, status:200})
                        }
                    });
            }else{
                response.json({message: 'Le mot de passe doit contenir au moins 8 caractères', ajoute: false, status:400});
                response.end();
            }

        } else {
            response.json({message: 'Le mot de passe et la vérification sont différentes', ajoute: false, status:400});
            response.end();
        }
    }else{
        response.json({message: 'Entrez tous les champs s\'il vous plaît', ajoute:false, status:400});
        response.end();
    }

});
app.post('/guideCherche', function(request, response) {
    const word = request.body.word;
    const word2="%"+word+"%"
    if(word) {
        con.query('select * from Conseil where (untitled LIKE ?) or (description LIKE ?)',[word2,word2] ,function (error, results, fields) {
            console.log("la")
            if (error) {
                console.log(error)
            }
            if (results.length > 0) {

                response.json(results)
            } else {
                response.json({fait:false})
            }
            response.end();
        });
    }

});

app.listen(PORT, () => {
    console.log(`Server listening on ${PORT}`);
});
