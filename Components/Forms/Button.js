import React from "react";
import {StyleSheet, Text, TouchableOpacity} from 'react-native'

const AppButton = ({ onPress, title, bgcolor, txtcolor}) => (
    <TouchableOpacity onPress={onPress} style={[styles.buttonContainer, {backgroundColor: bgcolor}]}>
      <Text style={{color: txtcolor, fontSize: 18}}>{title}</Text>
    </TouchableOpacity>
);

const styles = StyleSheet.create({
    buttonContainer:{
        margin: 10,
        borderRadius: 5,
        paddingVertical: 15,
        paddingHorizontal: 50,
        textAlign: "center",
        alignItems: "center",
        width: 334
    },
 
});
export default AppButton;