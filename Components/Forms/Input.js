import React from "react";
import {TextInput, StyleSheet, View} from 'react-native'

const Input = ({ placeholder, height=43 }) => (
    <View  style={[styles.inputContainer,{height: height}]}>
        <TextInput style = {styles.input} 
            placeholder = {placeholder}
            placeholderTextColor= "#C4C4C4"
        />
    </View>
);

const styles = StyleSheet.create({
    inputContainer:{
        borderWidth: 1,
        borderRadius: 10,
        paddingLeft: 20,
        marginBottom: 30
    },
    input:{
        borderRadius: 10,
        height: 43,
    }
 
});
export default Input;