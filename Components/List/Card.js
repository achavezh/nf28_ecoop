import React from "react"
import {Card, Paragraph} from "react-native-paper"
import {StyleSheet, TouchableOpacity} from 'react-native'


const CardCategory = ({title, bgcolor, description, navigation, screen, eventId, userMail}) =>{
    if(screen=='guide'){
        return (
            <TouchableOpacity onPress={()=>navigation.navigate('GuideCategory', {title: title })} >
                <Card style={[styles.card_style, {backgroundColor: bgcolor}]}>
                    <Card.Title title={title} titleStyle={styles.title_style} />
                    <Paragraph style={styles.paragraph}>
                        {description}
                    </Paragraph>
                                            
                </Card>
            </TouchableOpacity>
        );
    }else{
        return (
            
            <Card style={[styles.card_style, {backgroundColor: bgcolor}]}>
                    <Card.Title title={title} titleStyle={styles.title_style} />
                    <Paragraph style={styles.paragraph}>
                        {description}
                    </Paragraph>
                                            
                </Card>
        );
    }
}
const styles = StyleSheet.create({
    card_style:{
        marginBottom: 20,
        marginLeft:5,
        marginRight: 5
    },
    title_style:{
        color:"#FFFFFF",
        justifyContent:'center',
        textAlign:'center'
    },
    paragraph:{
        color:"#FFFFFF",
        textAlign:'justify',
        padding: 10
    }
});

export default CardCategory;