import React from "react";
import SimpleItem from "./SimpleItem";
import {View} from'react-native'
const SimpleList = ({questions}) =>{

    return questions.map((question, i)=>(
        <View style={{width:330}} key={i} >
            <SimpleItem question={question} />
        </View>

    ));
}

export default SimpleList;
