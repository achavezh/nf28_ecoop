import React from "react";
import SimpleItem from "./SimpleItem";
import {View} from'react-native'
import EventItem from "./EventItem";
const EventList = ({mail,evenements, navigation}) =>{

    return evenements.map((evenement, i) => (
        <View style={{width:"100%"}} key={i}>
            <EventItem mail={mail}evenement={evenement} navigation={navigation}/>
        </View>

    ));
}

export default EventList;
