import {View, Image, StyleSheet, Dimensions } from 'react-native'
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";

const EventImage = () => {
    return (
        <Image 
            source={{
                uri: 'https://picsum.photos/600/500'}} 
                style = {styles.image}
        />
    )
}

const dimensions = Dimensions.get('window');
const imageWidth = dimensions.width - 2 * responsiveFontSize(1.2);
const imageHeight = Math.round(imageWidth * 9 / 16);


const styles = StyleSheet.create({ 
    image: {
        height: imageHeight,
        width: imageWidth
    }
});

export default EventImage;