import React, {useState} from "react";
import {TouchableOpacity, View, StyleSheet, Text} from 'react-native'
import ArrowDown from "../Icons/ArrowDown";
import ArrowUp from "../Icons/ArrowUp";

const SimpleItem = ({question}) =>{
    const [shouldShow, setShouldShow] = useState(false);
    const [icon, setIcon] = useState(<ArrowDown style={styles.item_icon}/>);

    return (
        <View>
            <TouchableOpacity onPress={() =>{
                if(!shouldShow ){
                    setIcon(<ArrowUp style={styles.item_icon}/>)
                }else{
                    setIcon(<ArrowDown style={styles.item_icon}/>)
                }
                setShouldShow(!shouldShow)}

            }>
                <View style={styles.item}>

                    <View><Text style={styles.item_content}>{question.untitled}</Text></View>
                    <View >{icon}</View>

                </View>
                {shouldShow ?
                    (
                        <View >
                            <Text style={styles.information}>
                                {question.description}
                            </Text>
                        </View>
                    ) : null}
            </TouchableOpacity>
        </View>


    );
}
const styles = StyleSheet.create({
    item:{
        backgroundColor: "#89B9CA",
        padding: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 10,
        borderRadius: 5

    },
    item_content:{
        color: "#FFFFFF",
        fontSize: 18,
        width: 250
    },

    information:{
        textAlign:'justify',
        margin:10,
    }
});
export default SimpleItem;
