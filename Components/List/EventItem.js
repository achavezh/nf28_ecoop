import React, {useEffect, useState} from "react";
import {TouchableOpacity, View, StyleSheet, Text} from 'react-native'
import ParticipantIcon from "../Icons/ParticipantIcon";
import NonParticipantIcon from "../Icons/NonParticipantIcon";
import IconLike from "../Icons/IconLike";
import IconUnlike from "../Icons/IconUnlike";
import {useIsFocused} from "@react-navigation/native";
import {BASE_URL} from '../../Config/config';

const EventItem = ({mail,evenement, navigation}) =>{
    const [shouldParticipate, setShouldParticipate] = useState(evenement.participant);
    const[icon,setIcon]= useState(<NonParticipantIcon style={styles.item_icon}/>)
    const [shouldLike, setShouldLike] = useState(evenement.liked);
    const[icon2,setIcon2]= useState(<IconUnlike style={styles.item_icon}/>)
    const isFocused = useIsFocused();
    function inscrire() {
        if (shouldParticipate==null){
            fetch(`${BASE_URL}/inscrire`, {
                method: 'POST',
                headers: {
                    //   Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'access-control-allow-credentials': 'true'
                },
                /*   body: JSON.stringify({
                       mail: mail,
                       password: password
                   })*/
                body:JSON.stringify({"id":evenement.id,"mail":mail}),
            })
                .then(async res => {
                    try {
                        const jsonRes = await res.json();
                        if (res.status !== 200) {

                        } else {
                            setShouldParticipate(mail)
                        }
                    } catch (err) {
                        console.log(err);
                    };
                });
        }
        else{
            fetch(`${BASE_URL}/desinscrire`, {
                method: 'POST',
                headers: {
                    //   Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'access-control-allow-credentials': 'true'
                },
                /*   body: JSON.stringify({
                       mail: mail,
                       password: password
                   })*/
                body:JSON.stringify({"id":evenement.id,"mail":mail}),
            })
                .then(async res => {
                    try {
                        const jsonRes = await res.json();
                        if (res.status !== 200) {

                        } else {
                            setShouldParticipate(null)
                        }
                    } catch (err) {
                        console.log(err);
                    };
                });
        }
    }
    function like() {
        if (shouldLike==null){
            fetch(`${BASE_URL}/like`, {
                method: 'POST',
                headers: {
                    //   Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'access-control-allow-credentials': 'true'
                },
                /*   body: JSON.stringify({
                       mail: mail,
                       password: password
                   })*/
                body:JSON.stringify({"id":evenement.id,"mail":mail}),
            })
                .then(async res => {
                    try {
                        const jsonRes = await res.json();
                        if (res.status !== 200) {

                        } else {
                            setShouldLike(mail)
                        }
                    } catch (err) {
                        console.log(err);
                    };
                });
        }
        else{
            fetch(`${BASE_URL}/unlike`, {
                method: 'POST',
                headers: {
                    //   Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'access-control-allow-credentials': 'true'
                },
                /*   body: JSON.stringify({
                       mail: mail,
                       password: password
                   })*/
                body:JSON.stringify({"id":evenement.id,"mail":mail}),
            })
                .then(async res => {
                    try {
                        const jsonRes = await res.json();
                        if (res.status !== 200) {

                        } else {
                            setShouldLike(null)
                        }
                    } catch (err) {
                        console.log(err);
                    };
                });
        }
    }
    const onScreenLoad = () =>{
        if (shouldParticipate!=null){
            setIcon(<ParticipantIcon style={styles.item_icon}/>)
        }
        if(shouldParticipate==null){
            setIcon(<NonParticipantIcon style={styles.item_icon}/>)
        }
        if (shouldLike!=null){
            setIcon2(<IconLike style={styles.item_icon}/>)
        }
        if (shouldLike==null){
            setIcon2(<IconUnlike style={styles.item_icon}/>)
        }
    }
    useEffect(() => {
        onScreenLoad();
    }, [shouldParticipate])
    return (
        <View>
            <View style={styles.item}>
                <TouchableOpacity onPress={() => navigation.navigate('EventShow',{
                    eventId:evenement.id,
                    userMail:mail
                })}>
                    <View style={styles.line3}>
                        <Text style={styles.item_content}>{evenement.event}</Text>
                        <Text style={styles.item_content}>{evenement.lieu}</Text>


                    </View>
                    <View style={styles.line2}>
                        <TouchableOpacity onPress={() =>{
                            if(shouldLike!=null ){
                                like()
                                setIcon2(<IconUnlike style={styles.item_icon}/>)
                            }else{
                                like()
                                setIcon2(<IconLike style={styles.item_icon}/>)
                            }

                        }

                        }>
                            {icon2}
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() =>{
                            console.log(shouldParticipate)
                            if(shouldParticipate!=null ){
                                inscrire()
                                setIcon(<NonParticipantIcon style={styles.item_icon}/>)
                                setShouldParticipate(null)
                            }else{
                                inscrire()
                                setIcon(<ParticipantIcon style={styles.item_icon}/>)
                                setShouldParticipate(mail)
                            }

                        }

                        }>
                            <View >{icon}</View>
                        </TouchableOpacity>
                    </View>
                    </TouchableOpacity>

                </View>



        </View>


    );
}
const styles = StyleSheet.create({
    item:{
        backgroundColor: "#89B9CA",
        padding: 10,
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
        marginBottom: 3
    },
    item_content:{
        color: "#FFFFFF",
        fontSize: 18,
    },

    information:{
        textAlign:'justify',
        margin:10,
    },
    line3:{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    line2:{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
    }
});
export default EventItem;
