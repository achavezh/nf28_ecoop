import React from 'react'
import {View, Text, TouchableOpacity, StyleSheet} from'react-native'
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";

const DocumentaryEpisode = ({ title, content, navigation }) => {
  return (
    <View style = {styles.container}>
        <TouchableOpacity onPress={() => navigation.navigate('Episode', {content: content})} >
            <View style = {styles.episode}>
                <Text style = {styles.episode_title}>{title}</Text>
            </View>
        </TouchableOpacity>
    </View>
  )
};

const styles = StyleSheet.create({ 
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },

    episode: {
        width: responsiveWidth(90),
        backgroundColor: "#8FAD88",
        padding: 15,
        marginBottom: responsiveFontSize(1.5)
    },

    episode_title: {
        fontWeight: '700',
        fontSize: responsiveFontSize(2.5)
    },

});

export default DocumentaryEpisode