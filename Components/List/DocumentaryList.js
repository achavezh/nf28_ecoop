import React from 'react'
import DocumentaryEpisode from './DocumentaryEpisode';
import {View} from'react-native'

const DocumentaryList = ({ episodes, navigation }) => {
    return episodes.map((episode, i)=>(
        <View key={i}>
            <DocumentaryEpisode 
                title = {episode.episode_title}
                content = {episode.content}
                navigation = {navigation}
            />
        </View>

    ));
}

export default DocumentaryList