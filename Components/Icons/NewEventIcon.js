import React from "react";
import Svg, {Path}  from 'react-native-svg';
import {TouchableOpacity} from 'react-native';

const NewEventIcon = ({navigation, mail}) =>{
    
    return (
        <TouchableOpacity onPress={()=>navigation.navigate('NewEvent', {mail:mail})}>
            <Svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                <Path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm6 
                13h-5v5h-2v-5h-5v-2h5v-5h2v5h5v2z" fill="#000"/>
            </Svg>
        </TouchableOpacity>
    );
}

export default NewEventIcon;