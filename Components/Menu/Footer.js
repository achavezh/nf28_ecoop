import React, { useContext } from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {createNativeStackNavigator} from "@react-navigation/native-stack";
import { AuthContext } from '../../Context/AuthContext';

/** Icons imports */
import Logo from '../Icons/Logo'
import DocumentaryIcon from '../Icons/DocumentaryIcon';
import UserIcon from '../Icons/UserIcon';
import EventIcon from '../Icons/EventIcon';
import GuideIcon from '../Icons/GuideIcon';
import HomeIcon from '../Icons/HomeIcon';

/** Pages imports */
import Login from "../../Pages/Home/Login";
import Index from '../../Pages/Home/Index';
import UserAccount from '../../Pages/Home/UserAccount';
import ChangePassword from '../../Pages/Home/ChangePassword';
import SplashScreen from '../../Pages/Home/SplashScreen';
import Register from '../../Pages/Home/Register';
import Documentary from '../../Pages/Documentary/DocumentaryHome';
import Episode from '../../Pages/Documentary/Episode';
import Event from '../../Pages/Event/EventHome';
import EventShow from '../../Pages/Event/EventShow';
import NewEvent from '../../Pages/Event/NewEvent';
import GuideHome from '../../Pages/Guide/GuideHome';
import GuideCategory from '../../Pages/Guide/GuideCategory';


const Tab = createBottomTabNavigator();
const LoginStack = createNativeStackNavigator();
const HomeStack = createNativeStackNavigator();
const DocumentaryStack = createNativeStackNavigator();
const EventStack = createNativeStackNavigator();
const GuideStack = createNativeStackNavigator();

const Footer = () => {
    const {userInfo, splashLoading} = useContext(AuthContext);
  
    return (
        <NavigationContainer theme={styles}>
        {splashLoading ? (
            <LoginStack.Navigator>
                <LoginStack.Screen 
                    name = "SplashScreen" 
                    component={SplashScreen}
                    options = {{headerShown: false}}
                />
            </LoginStack.Navigator>
        ) : 
        userInfo.login ? (
            <Tab.Navigator screenOptions={{
                tabBarActiveBackgroundColor: '#2F5D62',
                tabBarInactiveBackgroundColor: '#2F5D62',
                tabBarActiveTintColor:"#FFF8D9",
                tabBarInactiveTintColor:"#FFFFFF",
                tabBarShowLabel: false,
                tabBarStyle :{
                    justifyContent: 'center',
                    height: 110,
                },
            }}>
                {/** Onglet 1. Accueil */}
                <Tab.Screen name = "AccueilTab"
                    options={({navigation}) => ({
                        headerShown: false,
                        tabBarIcon:()=>{
                            return (
                                <TouchableOpacity onPress={() => { 
                                    navigation.isFocused() ? navigation.navigate('Index') : navigation.navigate('AccueilTab')}}>
                                    <View style={icon.containerIcon} >
                                        <HomeIcon navigation={navigation} />
                                        <Text style={icon.iconText}>
                                            Accueil
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            );
                        }
                    })}
                > 
                {() => (
                    /** Header de l'onglet "Accueil" */
                    <HomeStack.Navigator screenOptions={({navigation}) => ({
                        headerStyle:{
                            backgroundColor: '#2F5D62'
                        },
                        headerRight: ()=><UserIcon navigation={navigation}></UserIcon>,
                        headerLeft:()=><Logo ></Logo>,
                        title: 'ECOOP',
                
                    })}>
                        {/** Toutes les pages (stacks) qui sont dans l'onglet "Accueil" */}
                        <HomeStack.Screen name = "Index" component = {Index} />
                        <HomeStack.Screen name = "EventShow" component={EventShow} />

                        <HomeStack.Screen name = "UserAccount" component = {UserAccount} />
                        <HomeStack.Screen name = "ChangePassword" component = {ChangePassword} />

                    </HomeStack.Navigator>
                )}
                </Tab.Screen>

                {/** Onglet 2. Documentary */}
                <Tab.Screen name = "DocumentaryTab"
                    options={({navigation}) => ({
                        headerShown: false,
                        tabBarIcon:()=>{
                            return (
                                <TouchableOpacity onPress={() => { navigation.isFocused() ? navigation.navigate('Documentary') : navigation.navigate('DocumentaryTab') }}>
                                    <View style={icon.containerIcon} >
                                        <DocumentaryIcon navigation={navigation} />
                                        <Text style={icon.iconText}>
                                            Documentaire
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            );
                        }
                    })}
                > 
                {() => (
                    /** Header de l'onglet "Documentary" */
                    <DocumentaryStack.Navigator screenOptions={({navigation}) => ({
                        headerStyle:{
                            backgroundColor: '#2F5D62'
                        },
                        headerRight: ()=><UserIcon navigation={navigation}></UserIcon>,
                        headerLeft:()=><Logo ></Logo>,
                        title: 'ECOOP',
                
                    })}>
                        {/** Toutes les pages (stacks) qui sont dans l'onglet "Documentary" */}
                        <DocumentaryStack.Screen name = "Documentary" component = {Documentary} />
                        <DocumentaryStack.Screen name = "Episode" component = {Episode} />
                        <DocumentaryStack.Screen name = "UserAccount" component = {UserAccount} />
                        <DocumentaryStack.Screen name = "ChangePassword" component = {ChangePassword} />
                    </DocumentaryStack.Navigator>
                )}
                </Tab.Screen>

                {/** Onglet 3. Events */}
                <Tab.Screen name = "EventsTab"
                    options={({navigation}) => ({
                        headerShown: false,
                        tabBarIcon:()=>{
                            return (
                                <TouchableOpacity onPress={() => { navigation.isFocused() ? navigation.navigate('Event') : navigation.navigate('EventsTab') }}>
                                    <View style={icon.containerIcon} >
                                        <EventIcon navigation={navigation} />
                                        <Text style={icon.iconText}>
                                            Évènements
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            );
                        }
                    })}
                > 
                {() => (
                    /** Header de l'onglet "Events" */
                    <EventStack.Navigator screenOptions={({navigation}) => ({
                        headerStyle:{
                            backgroundColor: '#2F5D62'
                        },
                        headerRight: ()=><UserIcon navigation={navigation}></UserIcon>,
                        headerLeft:()=><Logo ></Logo>,
                        title: 'ECOOP',
                
                    })}>
                        {/** Toutes les pages (stacks) qui sont dans l'onglet "Events" */}
                        <EventStack.Screen name = "Event" component = {Event} />
                        <EventStack.Screen name = "EventShow" component={EventShow} />
                        <EventStack.Screen name = "NewEvent" component={NewEvent} />
                        <EventStack.Screen name = "UserAccount" component = {UserAccount} />
                        <EventStack.Screen name = "ChangePassword" component = {ChangePassword} />
                    </EventStack.Navigator>
                )}
                </Tab.Screen>

                {/** Onglet 4. Guide */}
                <Tab.Screen name = "GuideTab"
                    options={({navigation}) => ({
                        headerShown: false,
                        tabBarIcon:()=>{
                            return (
                                <TouchableOpacity onPress={() => { navigation.isFocused() ? navigation.navigate('Guide') : navigation.navigate('GuideTab') }}>
                                    <View style={icon.containerIcon} >
                                        <GuideIcon navigation={navigation} />
                                        <Text style={icon.iconText}>
                                            Guide 
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            );
                        }
                    })}
                > 
                {() => (
                    /** Header de l'onglet "Guide" */
                    <GuideStack.Navigator screenOptions={({navigation}) => ({
                        headerStyle:{
                            backgroundColor: '#2F5D62'
                        },
                        headerRight: ()=><UserIcon navigation={navigation}></UserIcon>,
                        headerLeft:()=><Logo ></Logo>,
                        title: 'ECOOP',
                
                    })}>
                        {/** Toutes les pages (stacks) qui sont dans l'onglet "Guide" */}
                        <GuideStack.Screen name = "Guide" component = {GuideHome} />
                        <GuideStack.Screen name = "GuideCategory" component={GuideCategory} />
                        <GuideStack.Screen name = "UserAccount" component = {UserAccount} />
                        <GuideStack.Screen name = "ChangePassword" component = {ChangePassword} />
                    </GuideStack.Navigator>
                )}
                </Tab.Screen>

            </Tab.Navigator>      
        ) : (
            
            <LoginStack.Navigator screenOptions={() => ({
                headerStyle:{
                    backgroundColor: '#2F5D62'
                },
                headerLeft:()=><Logo ></Logo>,
                title: 'ECOOP',
            })}>
                <LoginStack.Screen 
                    name = "Login" 
                    component={Login}
                />
                <LoginStack.Screen 
                    name = "Register" 
                    component={Register}
                />
            </LoginStack.Navigator>
                    
        )}
        
        </NavigationContainer>
    )
}

const styles = {
    colors: {
        background: '#2F5D62',
        text : "#FFFFFF",
    }
}
const icon = StyleSheet.create({
    containerIcon:{
        flex: 1,
        textAlign: "center",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",

    },
    iconText:{
        color: "#ECEFEA",
        textAlign: "center",
        marginTop:10
    }

});

export default Footer;
