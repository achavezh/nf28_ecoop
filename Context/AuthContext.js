import React, {createContext, useState, useEffect} from 'react'
import axios from 'axios';
import {BASE_URL} from '../Config/config';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const AuthContext = createContext();

export const AuthProvider = ({children}) => {
    const [userInfo, setUserInfo] = useState({});
    const [isLoading, setIsLoading] = useState(false);
    const [splashLoading, setSplashLoading] = useState(false);

    const login = (mail, password) => {
        setIsLoading(true);

        axios.post(`${BASE_URL}/auth2`, {
            mail: mail,
            password: password
        },
        {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        })
        .then(res => {
            let userInfo = res.data;
            if (userInfo.success) {
                console.log("AuthContext: Login success")
            }
            else {
                console.log("AuthContext: Login not success")
            }
            setUserInfo(userInfo);
            AsyncStorage.setItem('userInfo', JSON.stringify(userInfo));
            setIsLoading(false);
        })
        .catch(e => {
            console.log(`login error ${e}`);
            setIsLoading(false);
        })
    };

    const logout = () => {
        setIsLoading(true);
        AsyncStorage.removeItem('userInfo');
        setUserInfo({});
        setIsLoading(false);
    };

    const isLoggedIn = async () => {
        try {
            setSplashLoading(true);
            let userInfo = await AsyncStorage.getItem('userInfo');
            userInfo = JSON.parse(userInfo);

            if (userInfo) {
                setUserInfo(userInfo);
            }
            

            setSplashLoading(false);
        } catch(e) {
            setSplashLoading(false);
            console.log(`is logged in error ${e}`);
        }
    }

    useEffect(() => {
        isLoggedIn();
    }, [])

    return (
        <AuthContext.Provider value = {{
            isLoading,
            userInfo,
            splashLoading,
            login,
            logout,
        }}>
            {children}
        </AuthContext.Provider>
    );
};
