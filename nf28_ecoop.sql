-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jun 07, 2022 at 12:03 AM
-- Server version: 5.7.34
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nf28_ecoop`
--

-- --------------------------------------------------------

--
-- Table structure for table `Category`
--

CREATE TABLE `Category` (
  `name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Category`
--

INSERT INTO `Category` (`name`) VALUES
('Energies'),
('Gaspillage'),
('Numerique'),
('Plastique'),
('Tri');

-- --------------------------------------------------------

--
-- Table structure for table `Conseil`
--

CREATE TABLE `Conseil` (
  `untitled` varchar(256) NOT NULL,
  `description` text,
  `categoryName` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Conseil`
--

INSERT INTO `Conseil` (`untitled`, `description`, `categoryName`) VALUES
('À quoi ça sert le tri ? ', 'Économiser, limiter notre impact environnemental, éviter de produire des déchets non recyclable. Le tri sélectif est bénéfique à plusieurs titres. Pour nos enfants, pour notre ville, pour notre environnement et notre nature, et surtout pour notre budget.\r\nVotre geste de tri contribue à économiser des ressources naturelles, à éviter le gaspillage et à limiter les pollutions.', 'Tri'),
('Comment trier tes déchets à la maison ?', '- Une poubelle ou un panier pour les emballages en verre\r\n- Une poubelle ou un sac pour les emballages et papiers\r\n- Un bioseau pour les déchets alimentaires\r\n- Une poubelle pour les ordures ménagères', 'Tri'),
('Conseils pour diminuer le plastique', '- Acheter en vrac\r\n- Préférer des cosmétiques solides\r\n- Préférer les grands conditionnements et les produits moins emballés\r\n- Boire de l’eau du robinet\r\n- Utiliser d’autres matières\r\n- Utiliser des produits rechargeables ou réutilisables\r\n- Toujours avoir un sac réutilisable à portée de main\r\n- Recycler tout ce que l’on n’a pas pu éviter', 'Plastique'),
('Conseils pour éviter le gaspillage alimentaire', '- Établir les menus de la semaine\r\n- Faire une liste de courses\r\n- Faire ses courses sans avoir faim\r\n- Lire et comprendre les dates de péremption\r\n- Respecter la chaîne du froid\r\n- Bien ranger son frigo\r\n- Conserver les aliments à la bonne température \r\n- Noter la date d’ouverture sur les produits\r\n- Cuisiner les bonnes quantités\r\n- Bien conserver et utiliser les restes', 'Gaspillage'),
('Éviter le gaspillage ...', 'En triant nos déchets, nous luttons activement contre le gaspillage. Jeter un emballage dans le bac de tri contribue à éviter le gaspillage, car une fois recyclé, un emballage sert à fabriquer de nouveaux objets. \r\nLe plastique recyclé sert à la fabrication de multitude d’objets : de nouvelles bouteilles, des jouets, des vêtements, etc. Avec le verre, ce sont des bouteilles, des pots et des bocaux qui sont récréés. L’acier et l’aluminium sont fondus pour former des pièces de voiture, des radiateurs, etc. Et le papier-carton sert à fabriquer d’autres objets ou emballages en papier-carton !', 'Tri'),
('La loi encore une fois ...', 'Le plastique à usage unique est présent à outrance dans notre quotidien. La loi anti-gaspillage pour une économie circulaire vise à réduire l’utilisation du plastique jetable et à favoriser la substitution du plastique par d’autres matériaux ou le développement d’emballages réutilisables ou recyclables et recyclés.\r\n\r\n', 'Plastique'),
('La pollution liée aux énergies fossiles serait responsable d\'un décès sur 5', 'La pollution provoquée par les énergies fossiles a été responsable de plus de 8 millions de morts prématurées en 2018, soit 20 % des adultes décédés dans le monde, ont estimé des chercheurs.\r\n\r\nSelon de précédentes recherches, la pollution de l\'air raccourcit de plus de deux ans l\'espérance de vie, en moyenne. Et l\'Asie est la plus touchée, avec une espérance de vie réduite de plus de 4 ans en Chine notamment, contre huit mois en Europe.\r\n\r\nSelon l\'Organisation mondiale de la Santé (OMS), la pollution de l\'air (qui inclut la pollution liée à la cuisson ou au chauffage dans les foyers) tue 7 millions de personnes par an, dont 4,2 millions liés à la pollution extérieure.\r\n\r\n', 'Energies'),
('Le déploiement de la 5G risque d’aggraver la pollution numérique\r\n', 'La 5G correspond à la cinquième génération des standards de téléphonie mobile. À volume de données égal la 5G exige moins d’énergie que la 4G. Néanmoins, cette efficacité ne compensera pas la forte hausse du volume de données transférées : on s\'attend à une augmentation notable de la consommation d\'électricité du secteur numérique.\r\nSelon une étude du Haut Conseil pour le climat (HCC), son déploiement entraînerait ainsi une augmentation de 18 à 45% de l’empreinte carbone du secteur numérique en France d’ici à 2030.', 'Numérique'),
('Le monde sous-marin d\'internet', 'Internet fonctionne aujourd\'hui grâce à des câbles sous-marins, qui sont enfouis au fond de tous les océans et mers de notre planète. Et ces câbles sous-marins ne sont pas deux ou trois mais début 2020, il seraient 406 câbles en fonctionnement. Le plus court fait 131 kilomètres tandis que le plus long près de 20 000 kilomètres ; il part de la Malaisie pour arriver en Californie.\r\n\r\nLes infrastructures réseau, représentent, selon l’Ademe, 28% des émissions de gaz à effet de serre émis par notre consommation du numérique. \r\n', 'Numérique'),
('Le mythe du recyclage', 'Seul 9 % du plastique mondial est recyclé. Même dans les pays développés, le taux de recyclage du plastique collecté par les ménages est souvent très inférieur à 50 %. Une très petite partie seulement de cette quantité est réutilisée pour fabriquer des emballages. La plupart des déchets d’emballage « recyclés » se retrouvent dans des produits de moindre valeur ou irrecyclables, ce qui signifie que ce processus a simplement pour effet de retarder l’inéluctable voyage vers la décharge.\r\n\r\nLes insuffisances de leur conception, l’absence d’infrastructures et de dispositif capable d’assurer le suivi des déchets en plastique sont autant de facteurs qui restreignent l’efficacité du recyclage et qui, à échéance prévisible, entérinent le destin de déchets de la plupart de ces emballages.', 'Plastique'),
('Le plastique et le changement climatique ...', 'Les émissions de gaz à effet de serre issues du cycle de vie des plastiques compromettent la capacité de la communauté mondiale à contenir la hausse de la température du Globe au-dessous de 1,5° C. Les plastiques sont fabriqués à plus de 90 % à partir de combustibles fossiles, et un rapport récent du CIEL (Center for Internation Environmental Law) estime que la pollution mondiale résultant de leur production et de leur incinération atteindra, pour la seule année 2019, le niveau des émissions de 189 centrales au charbon. Le même document estime que les rejets de gaz à effet de serre issus des plastiques pourraient représenter à l’horizon 2050 10 à 13 % de la totalité du budget carbone restant. La surproduction et la surconsommation de plastiques, si elles devaient continuer au même rythme, pourraient atteindre l’équivalent de 20 % de la consommation mondiale de pétrole d\'ici au milieu du siècle.\r\n\r\nSelon l’étude, les plastiques seuls pourraient consommer entre 10 et 13 % du carbone total dont nous disposons pour atteindre l’objectif de 1,5 degré. Si la production et la consommation de plastiques ne diminuent pas, 20 % de la consommation mondiale de pétrole sera attribuable aux plastiques d\'ici au milieu du siècle.', 'Plastique'),
('Le problème avec le plastique...', 'Certains plastiques resteraient dans l’environnement jusqu’à 1 000 ans. Pendant ce temps, ils se décomposent en morceaux de plus en plus petits, mais ils ne disparaissent pas complètement.\r\n\r\nOn constate que ces petits morceaux de plastique (microplastiques) peuvent être nocifs lorsqu’ils se répandent dans l’ensemble de la chaîne alimentaire. Ils peuvent même pénétrer notre corps par l’entremise des aliments que nous consommons.', 'Plastique'),
('Le tri chez les particuliers\r\n', 'Concernant les emballages en plastique, id’ici 2022 d’étendre à tous les foyers la possibilité de trier pour le recyclage l’ensemble des emballages plastiques (incluant les pots, les barquettes, les blisters) alors qu’auparavant seuls les bouteilles et flacons (récipients munis de flacons) devaient être triés.\r\nConcernant le tri des déchets occasionnels, celui-ci s’applique à une grande variété d’objets ou biens de consommation. Une partie de ces déchets sont concernés par le retour sur le lieu d’achat (ex : piles, petits appareils électriques, médicaments périmés).\r\nD’autres sont à déposer en déchèterie. La France compte 4500 déchèteries, qui reçoivent plus de 12 millions de tonnes de déchets par an. \r\n', 'Tri'),
('Les causes du gaspillage alimentaire ', 'Les causes sont aussi nombreuses que les acteurs de notre chaîne alimentaire. Les produits ne remplissant pas certains critères de taille, de forme ou d\'esthétique peuvent être simplement jetés lors de la production. Certaines manipulations ou imprévus de stockages inadéquats peuvent également mener à du rebut à de divers maillons de la chaîne de production. Toujours dans le domaine de la gestion de stocks, un excès de stock ou le déclenchement d’une date de péremption sont par ailleurs des causes de gaspillage. \r\n\r\nDu côté plus visible du consommateur, nous retrouvons le service de portions trop importantes dans la restauration privée ou collective, les erreurs de manipulation en cuisine, la mauvaise conservation des aliments ou - à nouveau - la péremption des denrées alimentaires. ', 'Gaspillage'),
('Les chiffres du gaspillage', 'Alimentaire :\r\n- 10 millions de tonnes de déchets alimentaires produits.\r\n- Un chiffre à retenir : 1/3 des aliments produits sur la planète sont jetés sans être consommés.\r\n- 20 à 30 kg d’aliments jetés par personne et par an en France, dont 7 kg encore emballés.\r\nNon alimentaire :\r\n- 630 millions d’euros de produits neufs détruits chaque année.', 'Gaspillage'),
('Les data centers, dévoreurs d’électricité au charbon', 'Composé de serveurs, le data center a pour rôle de stocker et traiter de grandes quantités de données. Pas n\'importe quelles données non, ce sont nos données soit des mails, photos, vidéos, jeux et celles des entreprises. Aujourd\'hui, on produirait actuellement 2,5 trillions d’octets par jour de données.\r\nEn Chine, les data centers sont à l’origine de 2,35 % de la consommation totale d’électricité du pays. Et contrairement à la France, l’électricité chinoise est produite au charbon. Les rejets de CO2 sont donc conséquents.', 'Numérique'),
('Les impacts pour la planète en chiffres sur le gaspillage…', '- 350 km3 d\'eau gaspillée\r\n- 1,3 milliard de tonnes de nourriture jetée\r\n- 1,4 million d\'hectares de terres gaspillés\r\n- 3,3 milliards de tonnes d\'émissions de CO2 chaque année.', 'Gaspillage'),
('Les produits alimentaires les plus gaspillés', 'Ce sont les fruits et les légumes qui sont le plus souvent gaspillés. Toujours selon les chiffres de l’ADEME (l\'Agence de la transition écologique), ils représentent en effet la moitié des denrées alimentaires jetées :\r\n\r\n- Légumes : 31 % ;\r\n- Boissons : 24 % ;\r\n- Fruits : 19 % ;\r\n- Riz et pâtes : 12 % ;\r\n- Pain : 4 % ;\r\n- Viandes et poissons : 4 % ;\r\n- Produits laitiers : 3 % ;\r\n- Plats préparés : 2 % ;\r\n- Produits sucrés : 1 %.', 'Gaspillage'),
('L’impact environnemental du numérique en quelques chiffres ', '- La phase de production des équipements numériques représente plus de 75% de l’empreinte environnementale du numérique (émissions de gaz à effet de serre, consommation d\'eau et de ressources). 88% des Français changent de téléphone portable alors que l’ancien fonctionne toujours. Entre 50 et 100 millions de téléphones dorment dans nos tiroirs. Pas moins de 70 matériaux différents, dont 50 métaux sont nécessaires pour fabriquer un smartphone.\r\n- Le flux de déchets d’équipements électriques et électroniques augmente de 2% par an en Europe.\r\n- Moins de 40% de ces déchets sont recyclés en Europe.', 'Numérique'),
('Plus de conseils pour réduire notre pollution numérique...', '- Allonger la durée de vie des équipements informatiques.\r\n- Vidéos : limiter la très haute définition.\r\n- Refuser les “objets connectés”\r\n- Éteindre votre box internet la nuit et durant vos absences.\r\n- Lutter contre les écrans vidéos publicitaires qui envahissent nos villes. ', 'Numérique'),
('Produits interdits à la vente pour lutter contre la pollution plastique', 'Depuis le 1er janvier 2020 :\r\n- vaisselle jetable en lot (verre, gobelet, assiettes)\r\n- coton-tige\r\nAu 1er janvier 2021\r\npailles :\r\n- couverts jetables\r\ntouillettes\r\n- couvercles des gobelets à emporter\r\npiques à steak\r\n- tiges pour ballons\r\n- confettis en plastique\r\n- boîtes en polystyrène expansé\r\ntous les objets en plastique oxodégradables\r\nEt au 1er janvier 2022\r\n- sachets de thé ou de tisane en plastique non biodégradable.', 'Plastique'),
('Qu\'est-ce que c\'est le gaspillage ?', 'Gaspillage est l\'action de mettre quelque chose en mauvais état, gâter quelque chose par manque d\'ordre, de dépenser de l\'argent sans discernement.', 'Gaspillage'),
('Que faire avec les déchets que je ne mets pas dans les bacs de tri ?', '- J’emmène à la déchèterie les objets encombrants et déchets dangereux (gravats, batterie, huile de vidange...), les déchets de jardinage (tonte de gazon, feuilles…).\r\n- Je ramène à la pharmacie les médicaments.\r\n- Je rapporte dans les supermarchés ou magasins mes piles et ampoules usagées.\r\n- Je donne mes vieux vêtements, jouets, mobiliers, appareils ménagers qui peuvent encore servir à des associations.\r\n- Pour les appareils électroniques, je ramène l’ancien appareil au vendeur qui le reprend gratuitement ou dans des conteneurs spécialisés, il y en a plein !', 'Tri'),
('Quelle est l’énergie fossile la plus et moins polluante ?', 'La plus : Le charbon, un combustible noir formé par l\'accumulation de débris végétaux transformés par la carbonisation, est considéré comme la source d’énergie la plus polluante. Si la production d’électricité, par exemple, à partir du charbon est plus intéressante d’un point de vue économique qu’à partir du pétrole ou du gaz, le charbon reste hautement polluant. En effet, le processus d’extraction dans les mines de charbon libère de grandes quantités de CO², en plus de polluer les nappes phréatiques, tandis que l’exploitation de cet hydrocarbure émettrait 3,5 tonnes de CO² par tonne d\'énergie consommée, soit nettement plus que le gaz et le pétrole.\r\nLa moins : Le gaz naturel est actuellement l\'énergie fossile la moins polluante, puisque sa combustion émet principalement de la vapeur d\'eau et une quantité réduite de CO2. Le gaz naturel génère en effet 30 à 50 % d\'émissions de CO2 en moins que les autres combustibles fossiles. \r\n', 'Energies'),
('Quelles sont les énergies non polluantes ?', 'Les énergies non polluantes sont les énergies dites renouvelables, issues de sources naturelles, disponibles en abondance sur notre planète et constamment renouvelées par la nature. Les principales énergies renouvelables sont l’énergie solaire (produite par le soleil), l’énergie éolienne (produite par la force du vent) et la biomasse (produite par la combustion d’éléments biologiques, tels que du bois ou des matières organiques). Contrairement aux énergies fossiles, les énergies renouvelables n’émettent pas de CO2. Malheureusement, la plupart des énergies renouvelables ne produisent qu’une seule énergie, l’électricité.\r\n\r\nMême si les énergies renouvelables sont plus respectueuses de notre environnement, il convient de souligner que, notamment du fait des processus d’exploitation, comme l’extraction ou le transport, aucune énergie n’est 100 % écologique.', 'Energies'),
('Quelques actions pour lier l\'écologie et numérique', 'Trier ses e-mails, éviter le cloud, utiliser un moteur écoresponsable, éteindre son ordinateur, fermer les onglets inactifs, changer la qualité de vidéo, passer à l\'hébergement vert.', 'Numérique'),
('Quelques chiffres de la pollution énergétique', 'Selon le GIEC (Groupe d\'experts intergouvernemental sur l\'évolution du climat), un quart des émissions de CO2 dans le monde est dû à la production d’énergie et principalement d’électricité.\r\n\r\nPas étonnant, quand on sait que 65 % de l’électricité mondiale est produite à partir d’énergies fossiles (charbon, pétrole, gaz).\r\n', 'Energies'),
('Quels sont les énergies polluantes ?', 'Les principales énergies polluantes sont les énergies fossiles, qui consomment des matières fossiles riches en carbone et en hydrogène, appelées les hydrocarbures. Les hydrocarbures les plus largement utilisés dans la production d’énergie sont le charbon, le pétrole et le gaz. Les énergies fossiles sont principalement utilisées pour le chauffage, le transport et l’industrie. Si elles sont efficaces et économiques, elles soulèvent deux problèmes majeurs :\r\n\r\n- Pollution : la combustion de ces sources d’énergie est responsable d’importantes émissions de gaz à effet de serre (CO2) et du réchauffement climatique.\r\n\r\n- Raréfaction des ressources : les hydrocarbures sont des ressources présentes en quantité limitée et non renouvelables, c’est-à-dire qu’elles ne peuvent pas être reconstituées à l\'échelle de temps humaine après leur utilisation.', 'Energies'),
('Qui contient une poubelle en général ?', '- Matières biodégradables 25%\r\n- Papiers cartons 21%\r\n- Divers 20%\r\n- Verre 11%\r\n- Plastiques 11%\r\n- Textiles 8%\r\n- Métaux 4%\r\n\r\nQuand tout est mélangé, on ne récupère rien ! Les déchets sont brulés ou mis en décharge.\r\n\r\nPour économiser les matières premières (bois, pétrole...), nous devons donc récupérer les déchets qui peuvent être recyclés. ', 'Tri'),
('Quoi dit la loi sur le gaspillage ?', 'La loi du 10 février 2020 relative à la lutte contre le gaspillage et à l’économie circulaire prévoit : « Au plus tard le 1er janvier 2022, il est mis fin à l\'apposition d\'étiquettes directement sur les fruits ou les légumes, à l\'exception des étiquettes compostables en compostage domestique et constituées en tout ou partie de matières biosourcées » (article 80).\r\nCette disposition a été ajoutée à la loi par un amendement considérant que les étiquettes apposées sur les fruits et légumes sont rarement compostables et qu’elles empêchent ainsi le compostage des restes des fruits et légumes et perturbent la qualité de la matière organique produits à l’issue des procédés de compostage.', 'Gaspillage'),
('Qu’est-ce que le tri ?', 'Le tri des déchets regroupe toutes les actions consistant à séparer et à récupérer les déchets selon leur nature pour les valoriser, ainsi qu\'à réduire au maximum la quantité de déchets ménagers résiduels*, non recyclables.\r\n\r\n* Déchets ménagers résiduels : part des déchets d’ordures ménagères qui reste après la collecte sélective.', 'Tri');

-- --------------------------------------------------------

--
-- Table structure for table `Duser`
--

CREATE TABLE `Duser` (
  `login` varchar(20) NOT NULL,
  `mail` varchar(256) NOT NULL,
  `password` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Duser`
--

INSERT INTO `Duser` (`login`, `mail`, `password`) VALUES
('UserTest1', 'a', 'mdpmdpmdpmdp'),
('Andrea11', 'Ada', 'mdp'),
('Andq', 'Andq', 'and'),
('Andrea', 'andrea99chavez61@gmail.com', 'mdp'),
('Andreaaaa', 'andrea99chavez61@gmail.commm', 'mdp'),
('ada', 'b', 'b'),
('da', 'c', 'c'),
('Demo', 'demo@mail.com', 'Aaaaaaaa'),
('UserTest2', 'Mail', 'a'),
('Miu', 'Miu', 'a'),
('quentin', 'qq@mail.com', 'd'),
('Afjdldmsñs', 'Skdksk', 'mdpmdpmdp'),
('Test user 2', 'User', 'user');

-- --------------------------------------------------------

--
-- Table structure for table `Eventa`
--

CREATE TABLE `Eventa` (
  `idEvent` int(11) NOT NULL,
  `nbParticipants` int(11) DEFAULT NULL,
  `beginsOn` date NOT NULL,
  `descirption` text,
  `likes` int(11) DEFAULT NULL,
  `organizer` varchar(256) DEFAULT NULL,
  `type` varchar(256) DEFAULT NULL,
  `street` varchar(64) DEFAULT NULL,
  `zipCODE` varchar(64) DEFAULT NULL,
  `town` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Eventa`
--

INSERT INTO `Eventa` (`idEvent`, `nbParticipants`, `beginsOn`, `descirption`, `likes`, `organizer`, `type`, `street`, `zipCODE`, `town`) VALUES
(31, 5, '2022-05-30', 'Je vais ramasser la', 0, 'Mail', 'Rammasage Déchet', 'Rue de tournis', '60200', 'Compiegne'),
(32, 3, '2022-05-30', 'Description', 0, 'Mail', 'Défilé', 'Rue QWERTY', '250120', 'Noyon'),
(33, 4, '2022-06-01', 'Nettoyer Paris, pas besoin de plus de détails. RDV dans le 92', 0, 'Mail', 'Rammasage Déchet', '16 rue de Bah', '99000', 'Paris'),
(35, 2, '2020-08-21', 'Vddd', 0, 'Miu', 'Réunion associative', 'Ma', 'Daa', 'Amiens'),
(39, 3, '2020-08-29', 'Description...', 0, 'a', 'Réunion associative', 'Rue test ', '452304', 'Rennes'),
(40, 4, '2020-09-19', 'Conjoncture', 0, 'a', 'Marche pour le climat', 'Bbb', 'Bbbjj', 'Compiegne '),
(41, 3, '2020-08-28', 'Description ', 0, 'a', 'Marche pour le climat', 'Rue BB', '172892', 'Compiegne'),
(42, 3, '2020-08-22', 'Nkaja', 0, 'qq@mail.com', 'Réunion associative', 'Test', '60200', 'Compiegne'),
(43, 5, '2021-03-26', 'Réunion sur le tri', 0, 'a', 'Réunion associative', 'Compiegne', '60200', 'Rue Roger coutllonc');

-- --------------------------------------------------------

--
-- Table structure for table `Likes`
--

CREATE TABLE `Likes` (
  `userID` varchar(256) NOT NULL,
  `Event` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Likes`
--

INSERT INTO `Likes` (`userID`, `Event`) VALUES
('a', 1),
('a', 31),
('a', 39);

-- --------------------------------------------------------

--
-- Table structure for table `participant`
--

CREATE TABLE `participant` (
  `userID` int(11) NOT NULL,
  `chat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `participant`
--

INSERT INTO `participant` (`userID`, `chat`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `Participate`
--

CREATE TABLE `Participate` (
  `participant` varchar(256) NOT NULL,
  `Event` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Participate`
--

INSERT INTO `Participate` (`participant`, `Event`) VALUES
('a', 31),
('a', 39),
('a', 43);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Category`
--
ALTER TABLE `Category`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `Conseil`
--
ALTER TABLE `Conseil`
  ADD PRIMARY KEY (`untitled`);

--
-- Indexes for table `Duser`
--
ALTER TABLE `Duser`
  ADD PRIMARY KEY (`mail`),
  ADD UNIQUE KEY `login` (`login`);

--
-- Indexes for table `Eventa`
--
ALTER TABLE `Eventa`
  ADD PRIMARY KEY (`idEvent`),
  ADD KEY `Eventa_ibfk_1` (`organizer`);

--
-- Indexes for table `Likes`
--
ALTER TABLE `Likes`
  ADD PRIMARY KEY (`userID`,`Event`);

--
-- Indexes for table `participant`
--
ALTER TABLE `participant`
  ADD PRIMARY KEY (`userID`,`chat`);

--
-- Indexes for table `Participate`
--
ALTER TABLE `Participate`
  ADD PRIMARY KEY (`participant`,`Event`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Eventa`
--
ALTER TABLE `Eventa`
  MODIFY `idEvent` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Eventa`
--
ALTER TABLE `Eventa`
  ADD CONSTRAINT `Eventa_ibfk_1` FOREIGN KEY (`organizer`) REFERENCES `Duser` (`mail`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
