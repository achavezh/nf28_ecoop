# NF28_Ecoop

## Membres de l'équipe :
- CHAVEZ HERREJON Andrea
- CISNEROS ARAUJO Ricardo
- FITTE-REY Quentin

## Architecture de l'application
- [Diagramme de classe](https://md.picasoft.net/s/tWSD8cvM9#)

## Technologies utilisées :
- Front-end :
    - React Native
- Back-end :
    - NodeJS v16.15 LTS
    - Express (à installer dans le repertoire du projet)

## Initialisation du projet

### 1. Cloner le projet depuis Gitlab : ([Installer git](https://github.com/git-guides/install-git))
```bash
git clone https://gitlab.utc.fr/achavezh/nf28_ecoop.git
```

### 2. Installer NodeJS et npm sur Linux
> Pour l'installation sur Windows ou Mac : https://nodejs.org/en/download/

```bash
curl -fsSL https://deb.nodesource.com/setup_16.x | sudo -E bash -
sudo apt install nodejs
```

On peut vérifier la bonne instalation avec via :
```bash
node --version
npm --version
```


### 3. Installer les dépendances du projet DANS le dossier du projet gitté :
```bash
cd nf28_ecoop/
npm install
```

Lancer le projet
```bash
npm start
```

### 4. Exécution
- Sur un téléphone
    - Télécharger l'application Expo client (android ou iOS)
    - Connecter ordinateur et téléphone sur le même réseau wifi
    - Puis :
        - android :  scanner le QR-code à partir de l'application expo
        - iOS : scanner le QR-code à partir de l'appareil photo
- Sur un émulateur
    - Device virtuel d'android studio
