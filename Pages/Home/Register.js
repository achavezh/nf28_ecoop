import React, {useState} from "react";
import {SafeAreaView, ScrollView, StyleSheet, Text, View, TextInput} from 'react-native'
import Button from '../../Components/Forms/Button'
import {responsiveFontSize} from "react-native-responsive-dimensions";
import { BASE_URL } from "../../Config/config";

const Register = ({navigation}) =>{
    const [mail, setMail] = useState('');
    const [username, setName] = useState('');
    const [password, setPassword] = useState('');
    const [verifPassword, setVerifPassword] = useState('');
    const [message, setMessage] = useState('');

    const onSubmitHandler = () => {
        fetch(`${BASE_URL}/registerUser`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'access-control-allow-credentials': 'true'
            },
            body:JSON.stringify({"mail":mail,"username": username,"password":password, "vpassword" : verifPassword}),
        })
            .then(async res => {
                try {
                    const jsonRes = await res.json();
                    if (jsonRes.status !== 200) {
                        setMessage(jsonRes.message);
                        console.log(jsonRes.message)
                    } else {
                        console.log(jsonRes.ajoute);
                        if(jsonRes.ajoute!=null && jsonRes.ajoute==true){
                            console.log("okk")
                            
                            navigation.navigate('Login')
                        }
                    }
                } catch (err) {
                    console.log(err);
                    setMessage(jsonRes.message);
                };
            })
            .catch(err => {
                console.log(err);
            });
    };

        return(
            <View style={styles.container}>
                <SafeAreaView >
                    <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>
                        <View style={styles.container}>
                            <Text style={styles.title}>Register</Text>
                            <Text style={{marginBottom:20, color: "red"}}>{message}</Text>
                            
                            <TextInput 
                                style = {styles.input} 
                                onChangeText={setName} 
                                placeholderTextColor= "#C4C4C4"
                                placeholder="Nom utilisateur"/>
                            
                            <TextInput 
                                style = {styles.input} 
                                onChangeText={setMail} 
                                placeholderTextColor= "#C4C4C4"
                                placeholder="Adresse mail"/>
                            
                            <TextInput 
                                style = {styles.input} 
                                onChangeText={setPassword} 
                                secureTextEntry={true}
                                placeholderTextColor= "#C4C4C4"
                                placeholder="Mot de passe"/>
                            
                            <TextInput 
                                style = {styles.input} 
                                onChangeText={setVerifPassword} 
                                secureTextEntry={true}
                                placeholderTextColor= "#C4C4C4"
                                placeholder="Verifier mot de passe"/>
                            
                            <Button 
                                onPress={onSubmitHandler}
                                title="S'inscrire"
                                color="#2F5D62"
                                bgcolor="#2F5D62"
                                txtcolor="#FFFFFF"
                            />
                    
                            <Button 
                                onPress={()=>navigation.navigate('Login')} 
                                title="Retour à l'accueil"
                                bgcolor="#DFEEEA"
                                txtcolor="#000000"
                            />
                        </View>
                </ScrollView>
            
            </SafeAreaView>
        </View>
            
        
        );
    
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: responsiveFontSize(1),
        backgroundColor: '#fff'
    },
    
    title:{
        marginTop: 50,
        marginBottom: 30,
        fontWeight: 'bold',
        fontSize: responsiveFontSize(4)
    },
    sub_title: {
        marginTop: 20,
        marginBottom: 20,
        paddingTop: responsiveFontSize(2),
        fontWeight: 'bold',
        fontSize: responsiveFontSize(2.8)
    },
    inputContainer:{
        borderWidth: 1,
        borderRadius: 10,
        paddingLeft: 20,
        marginBottom: 30,
        width: "100%"
    },
    input:{
        alignSelf: 'stretch',
        padding: 16,
        marginBottom: 20,
        backgroundColor: '#ededed'
    }
 
});
export default Register;
