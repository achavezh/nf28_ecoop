import React, {useContext, useState} from "react";
import {SafeAreaView, ScrollView, StyleSheet, Text, View, TextInput} from 'react-native'
import Button from '../../Components/Forms/Button'
import { AuthContext } from "../../Context/AuthContext";
import { BASE_URL } from "../../Config/config";
import {responsiveFontSize} from "react-native-responsive-dimensions";

const ChangePsw = ({navigation}) =>{
    const {userInfo, isLoading, logout} = useContext(AuthContext);
    const [mail, setMail] = useState('');
    const [password, setPassword] = useState('');
    const [verifPassword, setVerifPassword] = useState('');
    const [message, setMessage] = useState('');

    const onSubmitHandler = () => {
        fetch(`${BASE_URL}/changePassword`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'access-control-allow-credentials': 'true'
            },
            body:JSON.stringify({"mail":userInfo.mail, "username": userInfo.login,"password":password, "vpassword" : verifPassword}),
        })
            .then(async res => {
                try {
                    const jsonRes = await res.json();
                    if (jsonRes.status !== 200) {
                        setMessage(jsonRes.message);
                    } else {
                        console.log(jsonRes.ajoute);
                        if(jsonRes.ajoute!=null && jsonRes.ajoute==true){
                            console.log("okk")
                            logout();
                        }
                    }
                } catch (err) {
                    console.log(err);
                    setMessage(jsonRes.message);
                };
            })
            .catch(err => {
                console.log(err);
            });
    };

    return(
        <View style={styles.container}>
            <SafeAreaView >
                <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>
                    <View style={styles.container}>
                        <Text style={styles.title}>Changer le mot de passe</Text>
                            <Text style={{marginBottom:20, marginTop: 20, color: "red"}}>{message}</Text>
                            <Text style={styles.sub_title2}>Adresse mail (non modifiable ici)</Text>
                            <TextInput 
                                style = {styles.input_noneditable} 
                                onChangeText={setMail} 
                                editable ={false}
                                placeholderTextColor= "#a8a5a5"
                                defaultValue = {userInfo.mail}
                            />
                            
                            <TextInput 
                                style = {styles.input} 
                                onChangeText={setPassword} 
                                secureTextEntry={true}
                                placeholderTextColor= "#C4C4C4"
                                placeholder="Mot de passe"/>
                            
                            <TextInput 
                                style = {styles.input} 
                                onChangeText={setVerifPassword} 
                                secureTextEntry={true}
                                placeholderTextColor= "#C4C4C4"
                                placeholder="Verifier mot de passe"/>
                        
                            <Button 
                                onPress={onSubmitHandler} 
                                title="Modifier le mot de passe"
                                color="#2F5D62"
                                bgcolor="#2F5D62"
                                txtcolor="#FFFFFF"
                            />
                    
                            <Button 
                                onPress={()=>navigation.navigate('Index')} 
                                title="Retour à l'accueil"
                                bgcolor="#DFEEEA"
                                txtcolor="#000000"
                            />
                    </View>
            </ScrollView>
           
        </SafeAreaView>
    </View>
    );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: responsiveFontSize(1),
        backgroundColor: '#fff',

    },
    title:{
        marginTop: 50,
        fontWeight: 'bold',
        fontSize: responsiveFontSize(4)
    },
    sub_title: {
        marginTop: 20,
        marginBottom: 20,
        paddingTop: responsiveFontSize(2),
        fontWeight: 'bold',
        fontSize: responsiveFontSize(2.8)
    },
   
    input:{
        alignSelf: 'stretch',
        padding: 16,
        marginBottom: 20,
        backgroundColor: '#ededed'
    },
    input_noneditable:{
        alignSelf: 'stretch',
        padding: 16,
        marginBottom: 20,
        backgroundColor: '#d1cfcf'
    },
    sub_title2: {
        marginBottom: 10,
        paddingTop: responsiveFontSize(0),
        fontSize: responsiveFontSize(2.3)
    }
});
export default ChangePsw;