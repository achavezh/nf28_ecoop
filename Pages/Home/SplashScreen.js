import React from 'react'
import { View, ActivityIndicator } from 'react-native';

const SplashScreen = () => {
  return (
    <View style = {{flex: 1, justifyContent: 'center', backgroundColor: '#ccc'}}>
        <ActivityIndicator size="small" color="#000" />
    </View>
  )
}

export default SplashScreen;
