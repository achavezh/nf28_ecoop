import React, {useContext, useState} from "react";
import {SafeAreaView, ScrollView, StyleSheet, Text, View, TextInput} from 'react-native'
import Button from '../../Components/Forms/Button'

import {responsiveFontSize} from "react-native-responsive-dimensions";
import { AuthContext } from "../../Context/AuthContext";
import { BASE_URL } from "../../Config/config";

const UserAccount = ({navigation}) =>{
    const {userInfo, isLoading, logout} = useContext(AuthContext);
    const [name, setName] = useState('');
    const [mail, setMail] = useState('');
    const oldmail = userInfo.mail
    const oldlogin = userInfo.login
    
    const [message, setMessage] = useState('');
    

    const onSubmitHandler = () => {
        fetch(`${BASE_URL}/modifyUser`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'access-control-allow-credentials': 'true'
            },
            body:JSON.stringify({"oldmail": oldmail,"oldusername": oldlogin,"newmail":mail, "newusername" : name}),
        })
            .then(async res => {
                try {
                    const jsonRes = await res.json();
                    if (jsonRes.status !== 200) {
                        setMessage(jsonRes.message);
                        console.log(jsonRes.message)
                    } else {
                        console.log(jsonRes.ajoute);
                        if(jsonRes.ajoute!=null && jsonRes.ajoute==true){
                            logout();
                        }
                    }
                } catch (err) {
                    console.log(err);
                    setMessage(jsonRes.message);
                };
            })
            .catch(err => {
                console.log(err);
            });
    };
    return(
        <View style={styles.container}>
            <SafeAreaView >
                <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>
                    <View style={styles.container}>
                        <Text style={styles.title}>Mon Compte, {userInfo.login}</Text>
                        <Text style={styles.sub_title}>Mes informations</Text>
                        <Text style={{marginBottom:20, color: "red"}}>{message}</Text>
                        <Text style={styles.sub_title2}>Nom utilisateur</Text>

                        <TextInput 
                            onChangeText = {setName}
                            placeholder = {'Nom utilisateur courrant : '+userInfo.login}
                            placeholderTextColor = 'gray'
                            
                            style = {styles.textInput}
                        />
                        <Text style={styles.sub_title2}>Adresse mail</Text>

                        <TextInput 
                            onChangeText = {setMail}
                            placeholder = {'Mail courrant : '+userInfo.mail}
                            placeholderTextColor = 'gray'
                            style = {styles.textInput}

                        />

                        <Button 
                            onPress={onSubmitHandler}
                            title="Mettre à jour"
                            color="#2F5D62"
                            bgcolor="#2F5D62"
                            txtcolor="#FFFFFF"
                        />
                        <Button 
                            onPress={()=>navigation.goBack()} 
                            title="Retour à la page précédente"
                            bgcolor="#DFEEEA"
                            txtcolor="#000000"
                        />

                        <Button 
                            onPress={logout} 
                            title="Logout"
                            bgcolor="#DFEEEA"
                            txtcolor="#000000"
                        />

                        <View></View>
                        <Text style={styles.sub_title}>Paramètres</Text>
                        <Text style={styles.link}
                            onPress={()=>navigation.navigate('ChangePassword')}>
                            Changer mon mot de passe
                        </Text>
                        
                    </View>
                </ScrollView>
            </SafeAreaView>
        </View>
      
    );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: responsiveFontSize(1),
        backgroundColor: '#fff',

    },
    title:{
        marginTop: 50,
        fontWeight: 'bold',
        fontSize: responsiveFontSize(4)
    },
    sub_title: {
        marginTop: 10,
        marginBottom: 10,
        paddingTop: responsiveFontSize(2),
        fontWeight: 'bold',
        fontSize: responsiveFontSize(2.8)
    },
    sub_title2: {
        marginBottom: 10,
        paddingTop: responsiveFontSize(0),
        fontSize: responsiveFontSize(2.3)
    },
    link:{
        color: '#000000',
        fontSize: 15,
    },
    textInput:{
        alignSelf: 'stretch',
        padding: 16,
        marginBottom: 20,
        backgroundColor: '#ededed'
    }
 
});
export default UserAccount;