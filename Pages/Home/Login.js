import React, {useState, useContext, useEffect} from "react";
import {View, StyleSheet, Text, SafeAreaView, ScrollView} from 'react-native'
import Button from '../../Components/Forms/Button'
import {TextInput} from "react-native";
import { AuthContext } from '../../Context/AuthContext';
import Spinner from 'react-native-loading-spinner-overlay';

const Login = ({navigation}) =>{
    const [mail, setMail] = useState('');
    const [password, setPassword] = useState('');
    const {isLoading, login, userInfo} = useContext(AuthContext);

    return(
        <View style={styles.container}>
            <Spinner visible={isLoading} />
            <SafeAreaView >
                <View style={styles.container}>
                    <Text style={styles.title}>Connexion</Text>
                    <Text style={{marginBottom:20, color: "red"}}>{userInfo.message}</Text>

                    <TextInput 
                        onChangeText = {setMail} 
                        placeholder = 'Mail'
                        placeholderTextColor = 'gray'
                        style = {styles.textInput}
                    />
                    <TextInput 
                        onChangeText={setPassword} 
                        placeholder = 'Mot de passe'
                        placeholderTextColor = 'gray'
                        style = {styles.textInput}
                        secureTextEntry = {true}
                    />

                    <Button
                        onPress={ () => {
                            login(mail, password)}
                        }
                        title="Se connecter"
                        color="#2F5D62"
                        bgcolor="#2F5D62"
                        txtcolor="#FFFFFF"
                    />

                    <Button
                        onPress={()=>navigation.navigate('Register')}
                        title="S'inscrire"
                        bgcolor="#DFEEEA"
                        txtcolor="#000000"
                    />

                </View>
                
            </SafeAreaView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    title:{
        paddingBottom: 30,
        fontWeight: 'bold',
        fontSize: 40
    },
    textInput: {
        alignSelf: 'stretch',
        padding: 16,
        marginBottom: 20,
        backgroundColor: '#ededed'
    }

});

export default Login;
