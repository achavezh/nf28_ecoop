import React, {useContext, useEffect, useState} from "react";
import {SafeAreaView, ScrollView, View, StyleSheet, Text, TouchableOpacity} from 'react-native'
import CardCategory from "../../Components/List/Card";
import { ProgressBar} from "react-native-paper";
import {responsiveFontSize} from "react-native-responsive-dimensions";
import { AuthContext } from "../../Context/AuthContext";
import { BASE_URL } from "../../Config/config";
import {useIsFocused} from "@react-navigation/native";

const Index = ({navigation}) =>{
    const {userInfo} = useContext(AuthContext);
    const [myEvents, setMyEvents] = useState([]);
    const [partiEvents, setPartiEvents] = useState([]);
    const isFocused = useIsFocused();
    function partishow(){
        fetch(`${BASE_URL}/showEventParti`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'access-control-allow-credentials': 'true'
            },
            body: JSON.stringify({"mail": userInfo.mail}),
        })
            .then(async res => {
                try {
                    const jsonRes = await res.json();
                    if (jsonRes.success == true) {
                        setPartiEvents(jsonRes.res);
                        //console.log(jsonRes.res)
                    }
                } catch (err) {
                    console.log(err);
                }
                ;
            })
            .catch(err => {
                console.log(err);
            });
        fetch(`${BASE_URL}/showMyEvents`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'access-control-allow-credentials': 'true'
            },
            body: JSON.stringify({"mail": userInfo.mail}),
        })
            .then(async res => {
                try {
                    const jsonRes = await res.json();
                    if (jsonRes.success == true) {
                        setMyEvents(jsonRes.res);
                        console.log(jsonRes.res)
                    }
                } catch (err) {
                    console.log(err);
                }
                ;
            })
            .catch(err => {
                console.log(err);
            });
    }
    useEffect(() => {
        isFocused && partishow()
    },[isFocused]);

    return(

        <View style={styles.container}>
            <SafeAreaView >
                <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>
                    <View style={styles.container}>
                        <Text style={styles.title}>
                            Accueil
                        </Text>
                        <Text style={{ paddingTop: responsiveFontSize(2),
                            fontWeight: 'bold',
                            fontSize: responsiveFontSize(2.8),
                            marginBottom: 10}}>
                            Bienvenue {userInfo.login} !
                        </Text>
                        <Text style={styles.sub_title}>
                                Mes évènements
                        </Text>

                        <Text style={{margin: 20, fontSize:18, fontWeight: 'bold'}}>J'organise...</Text>
                        <ScrollView horizontal={true}>

                            <View style={styles.container_categories}>
                                {myEvents.map((item, index) => (
                                    <View key={index}>
                                        <TouchableOpacity onPress={() =>navigation.navigate('EventShow', {
                                            eventId: item.idEvent,
                                            userMail: userInfo.mail
                                        })} >
                                        <CardCategory title={item.type}
                                            bgcolor="#19647E"
                                            screen={"accueil"}
                                            description={item.beginsOn.replace(/T/, ' ').replace(/\..+/, '').split(/[- :]/)[2]+
                                            "/"+item.beginsOn.replace(/T/, ' ').replace(/\..+/, '').split(/[- :]/)[1]+
                                            "/"+item.beginsOn.replace(/T/, ' ').replace(/\..+/, '').split(/[- :]/)[0]
                                            +"\nÀ "+item.beginsOn.replace(/T/, ' ').replace(/\..+/, '').split(/[- :]/)[3]+
                                            ":"+item.beginsOn.replace(/T/, ' ').replace(/\..+/, '').split(/[- :]/)[4]
                                            +"\n"+item.town}
                                            />
                                        </TouchableOpacity>
                                    </View>
                                ))}

                            </View>
                        </ScrollView>

                        <Text style={{margin: 20, fontSize:18, fontWeight: 'bold'}}>Je participe...</Text>
                        <ScrollView horizontal={true}>

                            <View style={styles.container_categories}>
                                {partiEvents.map((item, index) => (
                                    <View key={index}>
                                        <TouchableOpacity onPress={() =>navigation.navigate('EventShow', {
                                            eventId: item.idEvent,
                                            userMail: userInfo.mail
                                        })} >
                                            <CardCategory title={item.type}
                                                          bgcolor="#19647E"
                                                          screen={"accueil"}
                                                          description={item.beginsOn.replace(/T/, ' ').replace(/\..+/, '').split(/[- :]/)[2]+
                                                              "/"+item.beginsOn.replace(/T/, ' ').replace(/\..+/, '').split(/[- :]/)[1]+
                                                              "/"+item.beginsOn.replace(/T/, ' ').replace(/\..+/, '').split(/[- :]/)[0]
                                                              +"\nÀ "+item.beginsOn.replace(/T/, ' ').replace(/\..+/, '').split(/[- :]/)[3]+
                                                              ":"+item.beginsOn.replace(/T/, ' ').replace(/\..+/, '').split(/[- :]/)[4]
                                                              +"\n"+item.town}
                                            />
                                        </TouchableOpacity>
                                    </View>
                                ))}

                            </View>
                        </ScrollView>

                    </View>
                </ScrollView>
            </SafeAreaView>

        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: responsiveFontSize(1),
        backgroundColor: '#fff'
    },
    title_container:{
        paddingBottom: responsiveFontSize(1),
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    title:{
        fontWeight: 'bold',
        fontSize: responsiveFontSize(4),
        marginTop: 20
    },
    sub_title: {
        paddingTop: responsiveFontSize(2),
        fontWeight: 'bold',
        fontSize: responsiveFontSize(2.8),
        marginBottom: 10
    },
    container_categories: {
        flex: 1,
        flexDirection: 'row',
    },

});

export default Index;
