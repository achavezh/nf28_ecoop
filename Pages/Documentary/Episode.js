import React from "react";
import {View, StyleSheet, Text, ScrollView} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";

const _renderItem = ({ item }) => {
    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1,  height: responsiveHeight(115)}}>
            <View style = {[ styles.slide, {backgroundColor: item.backgroundColor}, ]}>
                <Text style={styles.title}>{item.title}</Text>
                <Text style={styles.text}>{item.text}</Text>
            </View>
        </ScrollView>
    );
}

const Episode = ({navigation, route}) => {
    const content = route.params.content;
    return <AppIntroSlider renderItem={_renderItem} data={content} onDone = { () => navigation.goBack()} />; 
}

const styles = StyleSheet.create({
    slide: {
        flex: 1,
        padding: responsiveFontSize(3),
        resizeMode: 'cover',
        alignItems: 'center',
        //justifyContent: 'center',
    },

    title: {
        marginBottom: responsiveFontSize(1.5),
        fontWeight: 'bold',
        fontSize: responsiveFontSize(3.5),
    },

    text : {
        fontWeight: '400',
        fontSize: responsiveFontSize(2.5),
    },
  
});

export default Episode;