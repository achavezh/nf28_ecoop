import React from "react";
import {View, StyleSheet, Text, SafeAreaView, ScrollView, Button} from 'react-native';
import episodes from "./documentary.json";

import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";
import DocumentaryList from "../../Components/List/DocumentaryList";
    
const DocumentaryHome = ({navigation}) => {
    return(
        <View style={styles.container}>
            <SafeAreaView >
                <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>
                    <View style={styles.container}>
                        <View style={styles.title_container}>
                            <Text style={styles.title}>
                                Documentaire
                            </Text>
                        </View>

                        <Text style={styles.sub_title}>
                            Episodes
                        </Text>
                        
                        <DocumentaryList episodes = {episodes} navigation = {navigation}/>

                    </View>
                </ScrollView>    
            </SafeAreaView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: responsiveFontSize(1),
        backgroundColor: '#fff'
    },
    title_container:{
        paddingBottom: responsiveFontSize(1),
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    title:{
        fontWeight: 'bold',
        fontSize: responsiveFontSize(4)
    },

    sub_title: {
        paddingTop: responsiveFontSize(2),
        fontWeight: '500',
        fontSize: responsiveFontSize(3.5),
        marginBottom: responsiveFontSize(3)
    },
});

export default DocumentaryHome;