import React, {useEffect, useState} from "react";
import {View, StyleSheet, Text, ScrollView, SafeAreaView} from 'react-native'
import IdeaIcon from "../../Components/Icons/IdeaIcon";
import SimpleList from "../../Components/List/SimpleList";
import {BASE_URL} from '../../Config/config';

const GuideCategory = ({route, navigation}) =>{
    const title = route.params
   
    const [data, setData] = useState([]);
    fetch(`${BASE_URL}/nf28`+"?category="+title['title'])
        .then( (response) => {
            response.json().then(r => setData(r))

        })
        .catch(function (err) {
            console.log("Unable to fetch -", err);
        });

    return(
        <View style={styles.container}>
            <SafeAreaView >
                <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>
                    <View style={styles.container}>
                        <View style={styles.title_container}>
                            <Text style={styles.title}>
                                Mes conseils
                            </Text>
                            <IdeaIcon navigation={navigation}/>
                        </View>

                        <Text style={styles.sub_title}>
                            Category :  {title['title']}
                        </Text>
                        <SimpleList questions={data}/>
                    </View>

                </ScrollView>

            </SafeAreaView>
        </View>

    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    title_container:{
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    title:{
        fontWeight: 'bold',
        fontSize: 30,
        paddingRight: 10
    },
    sub_title:{
        paddingBottom: 10,
        paddingTop: 10,
        fontWeight: 'bold',
        fontSize: 25
    },


});

export default GuideCategory;
