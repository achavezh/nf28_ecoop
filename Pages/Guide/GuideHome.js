import React, {useEffect, useState} from "react";
import {View, StyleSheet, Text, ScrollView, SafeAreaView,} from 'react-native'
import IdeaIcon from "../../Components/Icons/IdeaIcon";
import { Searchbar } from "react-native-paper";
import CardCategory from "../../Components/List/Card";
import EventList from "../../Components/List/EventList";
import SimpleList from "../../Components/List/SimpleList";
import Geocoder from "react-native-geocoding";
import {BASE_URL} from "../../Config/config";

const GuideHome = ({navigation}) =>{
    const [searchQuery, setSearchQuery] = React.useState('');
    const [questions,setQuestions] = useState([]);
    const onChangeSearch = query => {
        if(query != "") {
            setSearchQuery(query)
            fetch(`${BASE_URL}/guideCherche`, {
                method: 'POST',
                headers: {
                    //   Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'access-control-allow-credentials': 'true'
                },
                body: JSON.stringify({"word": searchQuery}),
            })
                .then(async res => {
                    try {
                        const jsonRes = await res.json();
                        if (res.status !== 200) {

                        } else {
                            if(jsonRes.fait==null){
                                setQuestions(jsonRes)
                            }
                            else{
                                setQuestions([])
                            }
                        }
                    } catch (err) {
                        console.log(err);
                    }
                    ;
                })
                .catch(err => {
                    console.log(err);
                });
        }
        else{
            setSearchQuery("")
        }
    };


    if (searchQuery==""){
    return(
        <View style={styles.container}>
            <SafeAreaView >
                <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>
                    <View style={styles.container}>
                        <View style={styles.title_container}>
                            <Text style={styles.title}>
                                Mes conseils
                            </Text>
                            <IdeaIcon navigation={navigation}/>
                        </View>


                        <Searchbar
                            placeholder="Rechercher par mots clé"
                            style={styles.searchbar}
                            onChangeText={onChangeSearch}
                            value={searchQuery}

                        />

                        <Text style={styles.sub_title}>
                            Par catégories
                        </Text>
                        <View style={styles.container_categories}>
                            <View style={styles.item}>
                                <CardCategory title="Tri"
                                    bgcolor="#19647E"
                                    screen={"guide"}
                                    description={""}
                                    navigation={navigation}/>
                            </View>
                            <View style={styles.item}>
                                <CardCategory title="Energies"
                                    bgcolor="#FFD365"
                                    screen={"guide"}
                                    description={""}
                                    navigation={navigation}/>
                            </View>
                            <View style={styles.item}>
                                <CardCategory title="Numérique"
                                    bgcolor="#A42A32"
                                    screen={"guide"}
                                    description={""}
                                    navigation={navigation}/>
                            </View>
                            <View style={styles.item}>
                                <CardCategory title="Gaspillage"
                                    bgcolor="#4B4B4B"
                                    screen={"guide"}
                                    description={""}
                                    navigation={navigation}/>
                            </View>
                            <View style={styles.item}>
                                <CardCategory title="Plastique"
                                    bgcolor="#E2BCB7"
                                    screen={"guide"}
                                    description={""}
                                    navigation={navigation}/>
                            </View>

                        </View>
                    </View>

                </ScrollView>

            </SafeAreaView>
        </View>

    );}
    else{
        return(
            <View style={styles.container}>
                <SafeAreaView >
                    <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>
                        <View style={styles.container}>
                            <View style={styles.title_container}>
                                <Text style={styles.title}>
                                    Mes conseils
                                </Text>
                                <IdeaIcon navigation={navigation}/>
                            </View>


                            <Searchbar
                                placeholder="Rechercher par mots clé"
                                style={styles.searchbar}
                                onChangeText={onChangeSearch}
                                value={searchQuery}

                            />

                            <Text style={styles.sub_title}>
                                Par catégories
                            </Text>
                            <View style={styles.container_categories}>
                                <SimpleList questions={questions}></SimpleList>
                            </View>
                        </View>

                    </ScrollView>

                </SafeAreaView>
            </View>);
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 15,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    title_container:{
        paddingBottom: 40,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    title:{
        fontWeight: 'bold',
        fontSize: 30,
        paddingRight: 10
    },
    sub_title:{
        paddingBottom: 30,
        paddingTop: 30,
        fontWeight: 'bold',
        fontSize: 30
    },
    searchbar:{
        borderRadius: 10,
        width: 334,
        height: 43,
        marginBottom: 15
    },
    container_categories: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start',

    },
    item: {
        width: '50%',
    }

});

export default GuideHome;
