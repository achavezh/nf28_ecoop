import React, {Component, useState, useRef, useEffect} from 'react';
import {View, StyleSheet, Text, ScrollView, SafeAreaView, Dimensions, Image, Button} from 'react-native'
import EventImage from "../../Components/List/EventImage";
import {BASE_URL} from '../../Config/config';

import {responsiveFontSize} from "react-native-responsive-dimensions";


const EventShow = ({navigation, route}) =>{
    const id = route.params.eventId;
    const mail = route.params.userMail;
    const [type,setType]=useState("")
    const [participant,setParticipant]=useState(0)
    const [participant2,setParticipant2]=useState(0)
    const [adresse,setAdresse]=useState("")
    const [codePost,setCode]=useState("")
    const [ville,setVille]=useState("")
    const [description,setDescription]=useState("")
    const [bout,setBout]=useState(false)
    const [date,setDate]=useState("")

    function inscrire() {
        if (!bout){
            fetch(`${BASE_URL}/inscrire`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'access-control-allow-credentials': 'true'
                },
              
                body:JSON.stringify({"id":id,"mail":mail}),
            })
                .then(async res => {
                    try {
                        const jsonRes = await res.json();
                        if (res.status !== 200) {

                        } else {
                            setBout(true)
                        }
                    } catch (err) {
                        console.log(err);
                    };
        });
        }
        else{
            fetch(`${BASE_URL}/desinscrire`, {
                method: 'POST',
                headers: {
                   
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'access-control-allow-credentials': 'true'
                },
                
                body:JSON.stringify({"id":id,"mail":mail}),
            })
                .then(async res => {
                    try {
                        const jsonRes = await res.json();
                        if (res.status !== 200) {

                        } else {
                            setBout(false)
                        }
                    } catch (err) {
                        console.log(err);
                    };
                });
        }
    }
    useEffect(() => {
        fetch(`${BASE_URL}/showEvent`, {
        method: 'POST',
        headers: {
           
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'access-control-allow-credentials': 'true'
        },
       
        body:JSON.stringify({"id":id,"mail":mail}),
    })
        .then(async res => {
            try {
                const jsonRes = await res.json();
                if (res.status !== 200) {

                } else {
                    if(jsonRes.trouve==null ){
                        setType(jsonRes[0].type)
                        setCode(jsonRes[0].zipCODE)
                        setAdresse(jsonRes[0].street)
                        setDescription(jsonRes[0].descirption)
                        setVille(jsonRes[0].town)
                        setParticipant(jsonRes[0].nbParticipants)
                        setParticipant2(jsonRes[0].part)
                        var dateInter=new Date(jsonRes[0].beginsOn)
                        var dateInter2=dateInter.toISOString().replace(/T/, ' ').replace(/\..+/, '')
                        var time=dateInter2.split(/[- :]/);
                        console.log(time)
                        var dateFin=time[2]+"/"+time[1]+"/"+time[0]+ " à "+time[3]+":"+time[4]
                        setDate(dateFin)
                        if(jsonRes[0].partici==false){
                            setBout(false)
                        }
                        else{
                            setBout(true)
                        }
                    }
                    else{
                        navigation.goBack()
                    }
                }
            } catch (err) {
                console.log(err);
            };
        })
        .catch(err => {
            console.log(err);
        });
    }, [id,bout]);
    return(
        <View style={styles.container}>
            <SafeAreaView >
                <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>
                    <View style={styles.container}>
                        <View style={styles.title_container}>
                            <Text style={styles.title}>
                                Rejoindre l'évènement
                            </Text>
                        </View>

                        <Text style={styles.event_name}>
                            {type}
                        </Text>

                        <View style={styles.description}>
                            <View style={styles.description_container}>
                                <Text style={styles.description_title}>
                                    Description :
                                </Text>
                                <Text style={styles.description_content}>
                                    {description}
                                </Text>
                            </View>
                            
                        </View>

                        <Text style={styles.sub_title}>
                            Participants ({participant2}/{participant})

                        </Text>
                        <Text></Text>
                        {(() => {
                            if (bout){
                                return (
                                    <Button onPress={inscrire}title={"Se désinscrire"} color="#2F5D62"/>
                                )
                            }

                            else{
                                return(
                                <Button onPress={inscrire} title={"S'inscrire"} color="#2F5D62"/>
                                )
                            }
                        })()}

                        <Text style={styles.sub_title}>
                            Rendez-vous
                        </Text>
                        <Text style={styles.rendez_vous}>
                            Le {date}
                        </Text>
                        <Text style={styles.rendez_vous}>
                            {adresse}, {codePost} {ville}
                        </Text>

                        <Text style={styles.sub_title}>
                            Photographies du lieu
                        </Text>

                        <ScrollView horizontal={true}>
                            <View style={styles.event_images}>
                                <View>
                                    <EventImage />
                                </View>


                            </View>
                        </ScrollView>

                    </View>

                </ScrollView>

            </SafeAreaView>
        </View>

    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: responsiveFontSize(1),
        backgroundColor: '#fff'
    },
    title_container:{
        paddingBottom: responsiveFontSize(1),
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    title:{
        fontWeight: 'bold',
        fontSize: responsiveFontSize(4)
    },
    event_name:{
        paddingBottom: responsiveFontSize(2),
        fontWeight: '100',
        fontSize: responsiveFontSize(3.8)
    },

    description: {
        backgroundColor: "#4B4B4B",
        padding: responsiveFontSize(1),
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start'
    },

    description_container: {
        width: '60%'
    },

    event_creator: {
        width: '40%'
    },

    description_title: {
        color: "#fff",
        fontSize: responsiveFontSize(2.5)
    },
    description_content: {
        color: "#fff",
        fontSize: responsiveFontSize(1.8),
        textAlign: 'justify'
    },

    sub_title: {
        paddingTop: responsiveFontSize(2),
        paddingBottom: responsiveFontSize(2),
        fontWeight: 'bold',
        fontSize: responsiveFontSize(2.8)
    },

    rendez_vous: {
        fontSize: responsiveFontSize(2.5)
    },

    event_images: {
        flex: 1,
        flexDirection: 'row',
    },

});

export default EventShow;
