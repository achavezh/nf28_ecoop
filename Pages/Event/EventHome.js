import React, {useEffect, useState, useContext} from "react";
import {View, StyleSheet, Text, ScrollView, SafeAreaView, Dimensions,} from 'react-native'
import { Searchbar } from "react-native-paper";
import MapView, {PROVIDER_GOOGLE,Marker} from "react-native-maps";
import EventList from "../../Components/List/EventList";
import NewEventIcon from "../../Components/Icons/NewEventIcon";
import {useIsFocused, useNavigation} from '@react-navigation/native';
import Geocoder from 'react-native-geocoding';
import {GooglePlacesAutocomplete} from "react-native-google-places-autocomplete";

import { BASE_URL } from "../../Config/config";
import { AuthContext } from '../../Context/AuthContext';

const EventHome = ({navigation}) => {
    
    Geocoder.init("AIzaSyDO_BH-iNxHab6qrNpqronANyrWePmMWVA");
    const {userInfo, splashLoading} = useContext(AuthContext);
    const mailUser = userInfo.mail
    const [region,setRegion]=useState({
        latitude: 49.4038315,
        longitude: 2.8085288,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
    })
    const isFocused = useIsFocused();
    const [changement,setChangement]=useState(false)
    const [evenements,setEvenements] = useState([])
    const [staticData,setMarker] = useState([
        { coordinates: { latitude: 37.78383, longitude: -122.405766 } ,type:"Dechet"},
        { coordinates: { latitude: 37.78584, longitude: -122.405478 } ,type:"AD"},
        { coordinates: { latitude: 37.784738, longitude: -122.402839 } ,type:"SA"},
    ]);
    function nav(item){
       
    }
    useEffect(() => {
        isFocused &&
        fetch(`${BASE_URL}/showAllEvent`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'access-control-allow-credentials': 'true'
            },
            body:JSON.stringify({"mail":mailUser}),
        })
            .then(async res => {
                try {
                    const jsonRes = await res.json();
                    if (res.status !== 200) {

                    } else {
                        if(jsonRes.trouve==null ){
                            const events=[]
                            const mark=[]
                            jsonRes.map((item)=>(
                                events.push({"event": item.type,"lieu": item.town, "participant":item.participant,"liked": item.userID,"id": item.idEvent,"coor":""}),
                            Geocoder.from(item.town)
                                .then(json => {
                                    var location = json.results[0].geometry.location;
                                    mark.push({"coordinates": { "latitude": location.lat, "longitude": location.lng } ,"type":item.type,"id":item.idEvent})
                                    console.log(location);
                                    console.log(mark)

                                })
                            ))
                            setEvenements(events)
                            setMarker(mark)
                        }
                        else{
                            navigation.goBack()
                        }
                    }
                } catch (err) {
                    console.log("EVENT HOME : ", err);
                };
            })
            .catch(err => {
                console.log(err);
            });
    }, [mailUser,isFocused]);
    return(
        <View style={styles.container}>
            <SafeAreaView>
                <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'center'}} keyboardShouldPersistTaps="always">
                    <View style={styles.container}>
                        <View style={styles.title_container}>
                            <Text style={styles.title}>
                                Evènements
                            </Text>

                            <NewEventIcon navigation={navigation} mail={mailUser}/>

                        </View>
                        <GooglePlacesAutocomplete
                            style={styles.bar}
                            textInputProps={{
                                placeholderTextColor: 'gray',
                            }}
                            placeholder='Rentrez la ville...'
                            listViewDisplayed='auto'
                            fetchDetails={true}
                            onPress={(data, details = null) => {
                                // 'details' is provided when fetchDetails = true
                                console.log(data);
                                setRegion({latitude: details.geometry.location.lat, longitude: details.geometry.location.lng,latitudeDelta: 0.0922,
                                    longitudeDelta: 0.0421,})
                            }}
                            styles={{
                                container: {
                                    flex: 1,
                                    width:350
                                },
                                
                                textInput: {
                                    alignSelf: 'stretch',
                                    padding: 16,
                                    marginBottom: 20,
                                    backgroundColor: '#ededed',
                                },
                                predefinedPlacesDescription: {
                                    color: '#1faadb',
                                },
                            }}
                            query={{
                                key: "AIzaSyDO_BH-iNxHab6qrNpqronANyrWePmMWVA",
                                language:'en'
                            }}
                            GooglePlacesDetailsQuery={{
                                fields:'geometry'
                            }}
                        />

                        <View style={styles.map_container}>

                            <MapView
                                style={{
                                    width: 350,
                                    height: 300,
                                }}
                                minZoomLevel={2}
                                provider={PROVIDER_GOOGLE}
                                showsUserLocation={true}
                                region={region}
                                onRegionChangeComplete={setRegion}
                            >
                                {staticData.map((item, index) => (
                                    <Marker key={index} title={item.type} coordinate={item.coordinates}
                                            onCalloutPress={nav(item)}
                                    />
                                ))}

                            </MapView>
                        </View>

                        <View style={styles.container_categories}>
                                <EventList mail={mailUser} evenements={evenements} navigation={navigation}/>

                        </View>
                    </View>

                </ScrollView>

            </SafeAreaView>
        </View>

    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    title_container:{
        paddingBottom: 30,
        paddingTop: 30,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    title:{
        fontWeight: 'bold',
        fontSize: 30,
        paddingRight: "40%"
    },
    title2:{
        fontSize: 20,
        paddingRight: 30,
    },

    sub_title:{
        paddingBottom: 30,
        paddingTop: 30,
        fontWeight: 'bold',
        fontSize: 30
    },
    map_container:{
        padding: 10,
        width: "100%",
        marginBottom: 3,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "#ffffff"
    },
    container_categories: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'center',

    },
    bar:{
        textInputContainer: {
            width: '100%'
        },
        description: {
            fontWeight: 'bold'
        },
        predefinedPlacesDescription: {
            color: '#fff'
        },
    }

});

export default EventHome;
