import React, {useState, useContext} from "react";
import SelectDropdown from 'react-native-select-dropdown'
import {SafeAreaView, ScrollView, StyleSheet, Text, View} from 'react-native'
import Button from '../../Components/Forms/Button'
import NumericInput from 'react-native-numeric-input'

import {responsiveFontSize} from "react-native-responsive-dimensions";
import DatePicker from "@react-native-community/datetimepicker";
import {TextInput} from "react-native";
import {BASE_URL} from '../../Config/config';
import DateTimePicker from '@react-native-community/datetimepicker';

import { AuthContext } from '../../Context/AuthContext';

const NewEvent = ({navigation, route}) =>{
    const {userInfo, splashLoading} = useContext(AuthContext);
    const mail = userInfo.mail
    //console.log(userInfo)

    const [date, setDate] = useState(new Date(1598051730000));
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate;
        setShow(false);
        setDate(currentDate);
    };

    const showMode = (currentMode) => {
        setShow(true);
        setMode(currentMode);
    };

    const showDatepicker = () => {
        showMode('date');
    };

    const showTimepicker = () => {
        showMode('time');
    };
    const type_activite = ["Rammasage Déchet", "Marche pour le climat", "Réunion associative"]
    const [open, setOpen] = useState(false)
    const[error,setError]=useState("")
    const [type,setType]=useState(type_activite[1])
    const [participant,setParticipant]=useState(0)
    const [adresse,setAdresse]=useState("")
    const [codePost,setCode]=useState("")
    const [ville,setVille]=useState("")
    const [description,setDescription]=useState("")
    
    const onSubmitHandler = () => {
        fetch(`${BASE_URL}/creerEvent`, {
            method: 'POST',
            headers: {
                //   Accept: 'application/json',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'access-control-allow-credentials': 'true'
            },
            /*   body: JSON.stringify({
                   mail: mail,
                   password: password
               })*/
            body:JSON.stringify({"type":type,"participant":participant,"adresse":adresse,"codePost":codePost,"ville":ville,"description":description,"mail":mail,"date":date}),
        })
            .then(async res => {
                try {
                    const jsonRes = await res.json();
                    if (res.status !== 200) {

                    } else {
                        if(jsonRes.ajoute!=null && jsonRes.ajoute==true){
                            navigation.navigate('EventShow',{
                                eventId:jsonRes.id,
                                userMail:mail
                            })
                    }
                        else{
                            setError("Erreur, veuillez verifier les champs")
                        }
                    }
                } catch (err) {
                    console.log(err);
                };
            })
            .catch(err => {
                console.log(err);
            });
    };

    return(
        <View style={styles.container}>
        <SafeAreaView >
            <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>
                <View style={styles.container}>
                    <Text style={styles.title}>Organiser évènement</Text>
                    <Text style={{marginBottom:20, color: "red"}}>{error}</Text>
                    <Text style={styles.sub_title}>Activité prévue *</Text>
                    <SelectDropdown
                        data={type_activite}
                        onSelect={(selectedItem, index) => {
                            setType(selectedItem)
                        }}

                        buttonStyle={{
                            width: "100%",
                            marginBottom: 30
                        }}

                    />

                    <Text style={styles.sub_title}>Nombre de participants </Text>
                    <NumericInput type='up-down' minValue={0}
                        onChange={value => setParticipant(value)}
                        containerStyle={{width:"100%", marginBottom:20}} inputStyle={{width:300}}/>

                    <Text style={styles.sub_title}>Rendez-vous</Text>


                    <TextInput 
                        onChangeText={setAdresse} 
                        placeholder="Adresse *"
                        placeholderTextColor = 'gray'
                        style = {styles.textInput}/>
                    <TextInput 
                        placeholderTextColor = 'gray'
                        style = {styles.textInput}
                        onChangeText={setCode} 
                        placeholder="Code Postal *"/>
                    <TextInput 
                        onChangeText={setVille} 
                        placeholderTextColor = 'gray'
                        style = {styles.textInput}
                        placeholder="Ville *"/>

                    <TextInput 
                        onChangeText={setDescription} 
                        placeholderTextColor = 'gray'
                        style = {styles.textInput}
                        placeholder="Description" 
                        height={180}/>


                        <Button style={styles.button} onPress={showDatepicker} title="Selectionner une date" />
                        <Button style={{
                            margin: 10,
                            borderRadius: 5,
                            paddingVertical: 15,
                            paddingHorizontal: 50,
                            textAlign: "center",
                            alignItems: "center",
                            width: 334,
                            color:'#2F5D62'
                        }} onPress={showTimepicker} title="Selectionner une heure" />
                    {show && (
                        <DateTimePicker
                            placeholderText='DD/MM/YYYY'
                            dateFormat='dd/MM/yyyy'
                            testID="dateTimePicker"
                            value={date}
                            mode={mode}
                            is24Hour={true}
                            onChange={onChange}
                            style={styles.dateTimePicker}
                        />
                    )}


                    <Button
                        onPress={() =>onSubmitHandler()}
                        title="Publier l'évènement"
                        color="#2F5D62"
                        bgcolor="#2F5D62"
                        txtcolor="#FFFFFF"
                    />

                    <Button
                        onPress={()=>navigation.navigate('Event')}
                        title="Annuler"
                        bgcolor="#DFEEEA"
                        txtcolor="#000000"
                    />
                </View>
            </ScrollView>

            </SafeAreaView>
        </View>

    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: responsiveFontSize(1),
        backgroundColor: '#fff'
    },
    title_container:{
        paddingBottom: responsiveFontSize(1),
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
    },
    title:{
        fontWeight: 'bold',
        fontSize: responsiveFontSize(4),
        marginBottom: 30,
        marginTop: 30

    },
    dateTimePicker: {
        backgroundColor: '#2F5D62',
        borderRadius: 5,
        borderColor: '#2F5D62',
        borderWidth: 1,
        marginVertical: 10,
        height: 43,
    },
    button:{
        margin: 10,
        borderRadius: 5,
        paddingVertical: 15,
        paddingHorizontal: 50,
        textAlign: "center",
        alignItems: "center",
        width: 334,
        color:'#2F5D62'
    },
    sub_title: {
        fontSize: responsiveFontSize(2.4),
        marginBottom: 20
    },
    textInput: {
        alignSelf: 'stretch',
        padding: 16,
        marginBottom: 20,
        backgroundColor: '#ededed'
    }
});

export default NewEvent;
